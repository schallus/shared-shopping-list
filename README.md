# Shared Shopping List
**By Robin Zachmann & Florian Schaller**

## Planning
### Week 1
* Data model
* Mockups + Wireframing
* Get familiar with Angular
* Writing static HTML templates + CSS corresponding to the design / mockups
* Discuss on how to separate the tasks between the group members
* Define our API endpoints
* Setup our working environnment

### Week 2
* Encapsulate parts of the frontend application in Angular components
* Add functionality to the components using mock data
* Authentication (We will try to use OAuth 2.0)
* Routing
* Write the API Endpoints
* Write documentation for the implemented endpoints
* Error handling

### Week 3
* Connecting the frontend to the actual API endpoints
* Bugfixing
* Usability checks
* Adding extra functionnalities

**Last update: 19.04.2017**

## Project setup
1. Run (if necessary) > npm install -g @angular/cli
2. Setup local database
3. Copy and rename '.env_example' -> '.env' and set variables.
4. Run > `npm install`
5. Compile sass files for pug views (requires ruby)
   Go to directory: public/css/
   Run > `sass main.scss:main.css`
   Run > `sass bootstrap_custom.scss:bootstrap_custom.css`
   For both: `--watch` option to keep files up to date
6. Build project:
   Run > `ng build` (`--watch` option to keep files up to date)
7. Run > `npm start`

## Generate API documentation
`apidoc -i server/routes -o apidoc/ -f api.js`

## Heroku
Before to do anything else, install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)
Once installed, you can use the heroku command from your command shell.

###Add the Heroku remote repo
`git remote add heroku https://git.heroku.com/sssf-project.git`

### View logs
`heroku logs --tail`

### Run app
Run app locally: `heroku local`
Run app on Heroku server: `heroku open`

### Push local changes
1. `git add .`
2. `git commit -m "Demo"`
3. `git push heroku master`

### Start a console
`heroku run --app sssf-project bash`

## Generate API documentation

`apidoc -i server/routes -o apidoc/ -f "api.*"`