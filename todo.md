# Todo List
* Check image upload on Heroku
* Create global wallet
* On non-item related payment deletion, delete the linked item
* Style signup form, etc.
* Email notification on group invitation, etc.
* Add archive flag to items and optional parameter to the list items endpoint
* Websockets - update the list
* Sort items
* Problem with HTTPS redirection on root
