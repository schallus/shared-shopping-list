'use strict';

// ----- REQUIREMENTS -----

// vendor

// custom
const Group = require('../model/group');
const Invitation = require('../model/invitation');
const User = require('../model/user');

const invitation = {};

/**
 * @api {post} /web-api/v1/groups/:groupId/accept Accept an invitation
 * @apiName Accept invitation
 * @apiGroup Invitation
 *
 * @apiDescription Accept a group invitation to a group (join a group). You cannot join a group without being invited.
 *
 * @apiSuccess (200) {Object} group Group that you just joined.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "name": "Group Name",
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "image": "/uploads/img/1493379203923.jpg",
 *        "thumbnail": "/uploads/img/100x100_1493379203923.jpg",
 *        "members": [
 *          {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          {
 *            "_id": 2,
 *            "firstname": "Jane",
 *            "lastname": "Doe",
 *            "email": "jane.doe@domain.com"
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (404) {Object} NoInvitation There is no invitation to accept.
 * @apiError (404) {Object} WrongGroupId This group does not exist.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
invitation.acceptInvitation = (req, res, next) => {
  const user = res.locals.user;
  const groupId = +req.params.groupId;
  Group.findById(groupId).exec()
    .then((group) => {
      if (!group) {
        return Promise.reject({
          status: 404,
          message: 'This group does not exist.',
        });
      }
      return Invitation.findOne({user: user._id, group: groupId}).exec()
        .then((invitation) => {
          if (!invitation) {
            return Promise.reject({
              status: 404,
              message: 'There is no invitation to accept.',
            });
          }
          // Accept invitation
          if (group.members.indexOf(user._id) >= 0) {
            // User already part of group -> continue without adding user
            return Promise.resolve(group);
          } else {
            // Add a new group member
            const groupMembers = group.members;
            groupMembers.push(user._id);
            return Group.findByIdAndUpdate(
              groupId, {$set: {members: groupMembers}}, {new: true});
          }
        });
    })
    .then((updatedGroup) => {
      // Group successfully updated
      return Invitation.remove({user: user._id, group: groupId}).exec()
        .then(() => {
          return Promise.resolve(updatedGroup);
        });
    })
    .then((updatedGroup) => {
      // Invitation removed
      return updatedGroup
        .populate('creator', '_id firstname lastname email')
        .populate('members', '_id firstname lastname email')
        .execPopulate();
    })
    .then((populatedGroup) => {
      res.json({
        result: populatedGroup,
      });
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {post} /web-api/v1/groups/:groupId/reject Reject an invitation
 * @apiName Reject invitation
 * @apiGroup Invitation
 *
 * @apiDescription Reject a group invitation. The invitation will be deleted
 *    and you cannot restore it.
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *
 * @apiError (404) {Object} NoInvitation There is no invitation to reject.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "There is no invitation to reject"
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
invitation.rejectInvitation = (req, res, next) => {
  const user = res.locals.user;
  const groupId = +req.params.groupId;
  return Invitation.findOne({user: user._id, group: groupId}).exec()
    .then((invitation) => {
      if (!invitation) {
        return Promise.reject({
          status: 404,
          message: 'There is no invitation to reject.',
        });
      }
      // Remove invitation
      return Invitation.remove({user: user._id, group: groupId}).exec();
    })
    .then(() => {
      res.status(200).json({});
    })
    .catch((err) => {
      next(err);
    });
};

// Invitation removed


/**
 * @api {get} /web-api/v1/groups/invitations Get your group invitations
 * @apiName Get group invitations
 * @apiGroup Invitation
 *
 * @apiDescription Get an array containing your group invitations.
 *
 * @apiSuccess (200) {Object[]} invitations Your group invitations.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": [
 *        {
 *          "_id": 1,
 *          "date": "2017-04-27T14:31:24.286Z",
 *          "sender": {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Snow",
 *            "email": "john.snow@dead.com"
 *          },
 *          "user": {
 *            "_id": 2,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          "group": {
 *            "_id": 3,
 *            "name": "Name of the group"
 *          }
 *        }
 *      ]
 *    }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
invitation.listInvitations = (req, res, next) => {
  const user = res.locals.user;
  // Find user invitations
  Invitation.find({user: user._id})
    .populate('sender', '_id firstname lastname email')
    .populate('user', '_id firstname lastname email')
    .populate('group', '_id name')
    .exec()
    .then((invitations) => {
      res.status(200).json({
        result: invitations,
      });
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {post} /web-api/v1/groups/:groupId/invite Invite someone to a group
 * @apiName Invite someone to a group
 * @apiGroup Invitation
 *
 * @apiDescription Invite someone in a group. You have to be part of the group to grant the access to someone else.
 *
 * @apiParam {String} email Email address of the user to invite.
 *
 * @apiSuccess (200) {Object} invitation Invitation created.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "date": "2017-04-27T14:31:24.286Z",
 *        "sender": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "user": {
 *          "_id": 2,
 *          "firstname": "Jane",
 *          "lastname": "Doe",
 *          "email": "jane.doe@domain.com"
 *        },
 *        "group": {
 *          "_id": 2,
 *          "name": "John & Jane on Holiday"
 *        }
 *      }
 *    }
 *
 * @apiError (422) {Object} MissingParameters Please provide the user email address to invite.
 * @apiError (404) {Object} WrongGroupId This group does not exist.
 * @apiError (403) {Object} NotAllowed You are not allowed to invite someone to this group.
 * @apiError (404) {Object} WrongEmailAddress Email address not found.
 * @apiError (422) {Object} AlreadyMember The user is already member of the group.
 * @apiError (422) {Object} AlreadyInvited This user has already been invited to the group.
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
invitation.invite = (req, res, next) => {
  const me = res.locals.user;
  const groupId = +req.params.groupId;

  if (!req.body || !req.body.email) {
    return Promise.reject({
      status: 422, message: 'Please provide a user\'s email address to invite.',
    });
  }
  // Check if the group exist
  Group.findById(groupId).exec()
    .then((group) => {
      if (!group) {
        return Promise.reject({
          status: 404, message: 'This group does not exist.',
        });
      }
      // Check if the sender is part of the group
      if (group.members.indexOf(me._id) < 0) {
        return Promise.reject({
          status: 403,
          message: 'You are not allowed to invite someone to this group.',
        });
      }
      // Find the invited user by email
      return User.findByEmail(req.body.email)
        .then((user) => {
          // Check if user exist
          if (!user) {
            return Promise.reject({
              status: 404, message: 'Email address not found.',
            });
          }
          // Check if user is already in the group
          if (group.members.indexOf(user._id) >= 0) {
            return Promise.reject({
              status: 422,
              message: 'The user is already member of the group.',
            });
          }
          // Check if the user has already been invited
          return Invitation.findOne({user: user._id, group: group._id}).exec()
            .then((invitation) => {
              if (invitation) {
                return Promise.reject({
                  status: 422,
                  message: 'This user has already been invited to the group.',
                });
              }
              const newInvitation = {
                date: new Date(),
                sender: me._id,
                user: user._id,
                group: groupId,
              };

              // Create an invitation
              return Invitation.create(newInvitation);
            })
            .then((invitation) => {
              // Populate the invitation
              return invitation
                .populate('sender', '_id firstname lastname email')
                .populate('user', '_id firstname lastname email')
                .populate('group', '_id name')
                .execPopulate();
            })
            .then((invitation) => {
              res.status(200).json({
                result: invitation,
              });
            });
        });
    })
    .catch((err) => {
      next(err);
    });
};

module.exports = invitation;
