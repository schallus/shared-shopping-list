'use strict';

// ----- REQUIREMENTS -----

// vendor
const express = require('express');
const bodyParser = require('body-parser');
const timeout = require('connect-timeout');

// custom
const authCtrl = require('./api.auth');
const groupCtrl = require('./api.group');
const invitationCtrl = require('./api.invitation');
const itemCtrl = require('./api.item');
const paymentCtrl = require('./api.payment');

// Create API router
const router = new express.Router();

// ----- MIDDLEWARES -----

router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json({limit: '5mb'}));

/**
 * @apiDefine ServerTimeout
 *
 * @apiError (522) {Object} ConnectionTimeOut Connection Timed Out after 4 seconds.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 522 Connection Timed Out
 *     {
 *       "error": {
 *          "status": 522,
 *          "message": "Connection Timed Out."
 *       }
 *     }
 */
router.use(timeout('4s')); // After 4 seconds, connection time out

// ----- ROUTES -----

// ----- auth -----
router.post('/authenticate', authCtrl.authenticate);

// ----- group -----
router.route('/groups')
  .get(authCtrl.checkToken, groupCtrl.listGroups)
  .post(authCtrl.checkToken, groupCtrl.addGroup);
router.route('/groups/:groupId([0-9]+)')
  .get(authCtrl.checkToken, groupCtrl.getInformation)
  .patch(authCtrl.checkToken, groupCtrl.editGroup);
router.post('/groups/:groupId([0-9]+)/leave',
  authCtrl.checkToken, groupCtrl.leave);

// ----- invitation -----
router.get('/groups/invitations',
  authCtrl.checkToken, invitationCtrl.listInvitations);
router.post('/groups/:groupId([0-9]+)/accept',
  authCtrl.checkToken, invitationCtrl.acceptInvitation);
router.post('/groups/:groupId([0-9]+)/reject',
  authCtrl.checkToken, invitationCtrl.rejectInvitation);
router.post('/groups/:groupId([0-9]+)/invite',
  authCtrl.checkToken, invitationCtrl.invite);

// ----- item -----
router.route('/groups/:groupId([0-9]+)/items')
  .get(authCtrl.checkToken, itemCtrl.listItems)
  .post(authCtrl.checkToken, itemCtrl.addItem);
router.route('/groups/:groupId([0-9]+)/items/:itemId([0-9]+)')
  .delete(authCtrl.checkToken, itemCtrl.deleteItem)
  .patch(authCtrl.checkToken, itemCtrl.updateItem);
router.post('/groups/:groupId([0-9]+)/items/:itemId([0-9]+)/archive',
  authCtrl.checkToken, itemCtrl.archiveItem);

// ----- payment -----
router.route('/groups/:groupId([0-9]+)/payments')
  .post(authCtrl.checkToken, paymentCtrl.addPayment)
  .get(authCtrl.checkToken, paymentCtrl.listPayments);
router.route('/groups/:groupId([0-9]+)/payments/:paymentId([0-9]+)')
  .put(authCtrl.checkToken, paymentCtrl.editPayment)
  .delete(authCtrl.checkToken, paymentCtrl.deletePayment);
router.get('/groups/:groupId([0-9]+)/balance',
  authCtrl.checkToken, paymentCtrl.balance);

router.all('*', (req, res, next) => {
  next({
    status: 404,
    message: 'This endpoint doesn\'t exist',
  });
});

// ----- ERROR MIDDLEWARE -----

router.use((err, req, res, next) => {
  console.log(err);
  // map unexpected errors to default format
  if (!err.status || !err.message) {
    return res.status(500).json({
      error: {
        status: 500,
        message: 'Something unexpected happened',
      },
    });
  }
  if (err.code === 'ETIMEDOUT') {
    return res.status(522).json({
      error: {
        status: 522,
        message: 'Connection Timed Out.',
      },
    });
  }
  res.status(err.status).json({
    error: {
      status: err.status,
      message: err.message,
    },
  });
});

module.exports = router;
