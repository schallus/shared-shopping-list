'use strict';

// ----- REQUIREMENTS -----

// vendor
const multer = require('multer');

// custom
const Group = require('../model/group');

// Set up image upload
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'public/uploads/img');
  },
  filename: (req, file, cb) => {
    const fileName = +new Date() + '_' + file.originalname;
    cb(null, fileName);
  },
});
const upload = multer({storage: storage});
const uploadImage = upload.single('image');

const group = {};

/**
 * @api {get} /web-api/v1/groups List my groups
 * @apiName List my groups
 * @apiGroup Group
 *
 * @apiDescription List all the groups where the user belong.
 *
 * @apiSuccess (200) {Object[]} groups Array of groups where the user belong.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *     "result": [
 *      {
 *        "_id": 1,
 *        "name": "Group Name",
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "members": [
 *          {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          }
 *        ]
 *      }
 *     ]
 *    }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
group.listGroups = (req, res, next) => {
  const user = res.locals.user;
  Group.find({members: user._id})
    .populate('creator', '_id firstname lastname email')
    .populate('members', '_id firstname lastname email')
    .exec()
    .then((groups) => {
      res.status(200).json({
        result: groups,
      });
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {post} /web-api/v1/groups Create a new group
 * @apiName Create group
 * @apiGroup Group
 *
 * @apiDescription Create a new group.
 *
 * @apiParam {String} name Name of the group to create.
 * @apiParam {File} image Group image.
 *
 * @apiSuccess (200) {Object} group Group that the user created.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *     "result": {
 *        "_id": 1,
 *        "name": "Group Name",
 *        "image": "http://domain.com/uploads/img/1493303020466.jpg",
 *        "thumbnail": "http://domain.com/uploads/img/100x100_1493303020466.jpg",
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "members": [
 *          {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (422) {Object} MissingParameters Please provide a name and a group image.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "error": {
 *          "status": 422,
 *          "message": "Please provide a name and a group image."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
group.addGroup = (req, res, next) => {
  uploadImage(req, res, (err) => {
    if (err) {
      // An error occurred when uploading
      return next(err);
    }
    // Everything went fine
    const image = req.file;
    const user = res.locals.user;

    if (!req.body || !req.body.name || !image) {
      return next({
        status: 422,
        message: 'Please provide a name and a group image.',
      });
    }

    Group.create({name: req.body.name, creator: user._id, members: [user._id]})
      .then((group) => {
        // Once group created, we add the thumbnail
        return group.createThumbnail(image, 100, 100);
      })
      .then((group) => {
        // Image added success
        group.thumbnail = `${req.protocol}://${req.get('host')}${group.thumbnail}`;
        group.image = `${req.protocol}://${req.get('host')}${group.image}`;
        group.creator = user;
        group.members = [user];
        res.status(200).json({
          result: group,
        });
      })
      .catch((err) => {
        next(err);
      });
  });
};

/**
 * @api {get} /web-api/v1/groups/:groupId Get group information
 * @apiName Get group information
 * @apiGroup Group
 *
 * @apiDescription Get group information.
 *
 * @apiSuccess (200) {Object} group Information about the group requested.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *         "_id": 1,
 *         "name": "Group Name",
 *         "creator": {
 *           "_id": 1,
 *           "firstname": "John",
 *           "lastname": "Doe",
 *           "email": "john.doe@domain.com"
 *         },
 *         "members": [
 *           {
 *             "_id": 1,
 *             "firstname": "John",
 *             "lastname": "Doe",
 *             "email": "john.doe@domain.com"
 *           }
 *         ]
 *       }
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
group.getInformation = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;
  Group.find({
    _id: groupId,
    members: user._id,
  })
    .populate('creator', '_id firstname lastname email')
    .populate('members', '_id firstname lastname email')
    .exec()
    .then((group) => {
      if (!group.length > 0) {
        return Promise.reject({
          status: 404,
          message: 'This group does not exist.',
        });
      }
      res.status(200).json({
        result: group[0],
      });
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {patch} /web-api/v1/groups/:groupId Edit a group
 * @apiName Edit a group
 * @apiGroup Group
 *
 * @apiDescription Edit a group.
 *
 * @apiParam {String} [name] New group name.
 * @apiParam {File} [image] New group image.
 *
 * @apiSuccess (200) {Object} group Group which has been updated.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *         "_id": 1,
 *         "name": "Group Name",
 *         "image": "http://domain.com/uploads/img/1493303020466.jpg",
 *         "thumbnail": "http://domain.com/uploads/img/100x100_1493303020466.jpg",
 *         "creator": {
 *           "_id": 1,
 *           "firstname": "John",
 *           "lastname": "Doe",
 *           "email": "john.doe@domain.com"
 *         },
 *         "members": [
 *           {
 *             "_id": 1,
 *             "firstname": "John",
 *             "lastname": "Doe",
 *             "email": "john.doe@domain.com"
 *           }
 *         ]
 *       }
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist.
 * @apiError (422) {Object} MissingParameters Nothing to update. Please provide a name or an image.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
group.editGroup = (req, res, next) => {
  uploadImage(req, res, (err) => {
    if (err) {
      // An error occurred when uploading
      return next('An error occurred when uploading');
    }
    // Everything went fine
    const image = req.file;
    const user = res.locals.user;
    const groupId = req.params.groupId;

    if (!req.body.name && !image) {
      return next({
        status: 422,
        message: 'Nothing to update. Please provide a name or an image.',
      });
    }

    Group.findOne({_id: groupId, members: user._id}).exec()
      .then((group) => {
        if (!group) {
          return Promise.reject({
            status: 404,
            message: 'This group does not exist or you are not part of this group',
          });
        }
        // update name if body.name is defined
        if (req.body.name) {
          const newName = req.body.name.trim();
          return Group.findByIdAndUpdate(
            groupId, {$set: {name: newName}}, {new: true}).exec();
        } else {
          return Promise.resolve(group);
        }
      })
      .then((group) => {
        // Group name updated
        return image
          ? group.createThumbnail(image, 100, 100)
          : Promise.resolve(group);
      })
      .then((group) => {
        // Thumbnail created
        // Add protocol + host to images URL
        group.thumbnail = `${req.protocol}://${req.get('host')}${group.thumbnail}`;
        group.image = `${req.protocol}://${req.get('host')}${group.image}`;
        return group
          .populate('members', '_id firstname lastname email')
          .populate('creator', '_id firstname lastname email')
          .execPopulate();
      })
      .then((group) => {
        // Populate success
        return res.status(200).json({
          result: group,
        });
      })
      .catch((err) => {
        next(err);
      });
  });
};

/**
 * @api {post} /web-api/v1/groups/:groupId/leave Leave a group
 * @apiName Leave a group
 * @apiGroup Group
 *
 * @apiDescription Leave a group. Be careful ! If you are the last member
 *    to leave the group, this group may be deleted with all the items that
 *    it contains.
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *
 * @apiError (404) {Object} WrongGroupId The group for the given groupId does
 *      not exist, or the authenticated user is not part of this group.
 * @apiError (404) {Object} NotMember You are not member of this group.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not part
 *            of this group"
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
group.leave = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;

  Group.findOne({_id: groupId, members: user._id}).exec()
    .then((group) => {
      if (!group) {
        // The group does not exist
        return Promise.reject({
          status: 404,
          message: 'This group does not exist or you are not part of this group',
        });
      }
      if (group.members.indexOf(user._id) < 0) {
        // User was not in the group
        return Promise.reject({
          status: 404,
          message: 'You are not member of this group.',
        });
      }
      const members = group.members;
      members.splice(members.indexOf(user._id), 1);
      return Group.findByIdAndUpdate(groupId, {members: members});
    })
    .then((group) => {
      // Group successfully updated
      res.status(200).json({});
      // delete group if not items or members are related
      // todo
    })
    .catch((err) => {
      next(err);
    });
};

module.exports = group;
