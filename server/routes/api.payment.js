'use strict';

// ----- REQUIREMENTS -----

// vendor

// custom
const Group = require('../model/group');
const Payment = require('../model/payment');
const ShoppingItem = require('../model/shopping-item');

const payment = {};

const checkPaymentItems = (arrayItems, groupId) => {
  // Check if all the items exist in this group
  return ShoppingItem.find({group: groupId, checked: false})
    .exec()
    .then((items) => {
      const itemsIds = items.map((item) => item._id);

      for (let item of arrayItems) {
        if (itemsIds.indexOf(item) < 0) {
          console.log('Item ' + item + ' does not exist in the group');
          return Promise.reject({
            status: 422,
            message: 'Some of the item does not exist or are already checked.',
          });
        }
      }

      items = items.filter((obj) => arrayItems.indexOf(obj._id) > -1);

      return ShoppingItem.update(
        {_id: {$in: arrayItems}},
        {checked: true},
        {multi: true, new: true}
      ).then((result) => {
        if (result.ok) {
          items = items.map((item) => {
            item.checked = true;
            return item;
          });
          return Promise.resolve(items);
        }
      });
    });
};

const createPaymentItem = (name, creator, groupId, members) => {
  const newShoppingItem = {
    name: name,
    creator: creator,
    added: new Date(),
    group: groupId,
    members: members,
    checked: true,
    archived: false,
    payment: true,
  };


  return ShoppingItem.create(newShoppingItem).then((item) => {
    return Promise.resolve([item]);
  });
};

/**
 * @api {post} /web-api/v1/groups/:groupId/payments Create a payment
 * @apiName Create a payment
 * @apiGroup Payment
 *
 * @apiDescription Create a payment.
 *
 * @apiParam {Number[]} [items] Items related to the payment.
 * @apiParam {Number[]} [members] Members related to the payment.
 * @apiParam {String} [title] Title of the payment.
 * @apiParam {Number} [amount] Amount of the payment.
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "group": 1,
 *        "edited": "2017-05-01T07:30:26.600Z",
 *        "date": "2017-05-01T07:30:26.600Z",
 *        "amount": 324,
 *        "title": "Hotel Booking",
 *        "items": [
 *          {
 *            "_id": 3,
 *            "name": "Hotel Booking",
 *            "creator": {
 *              "_id": 1,
 *              "firstname": "John",
 *              "lastname": "Doe",
 *              "email": "john.doe@domain.com"
 *            },
 *            "added": "2017-05-01T07:30:26.473Z",
 *            "group": 1,
 *            "checked": true,
 *            "members": [
 *              {
 *                "_id": 1,
 *                "firstname": "John",
 *                "lastname": "Doe",
 *                "email": "john.doe@domain.com"
 *              },
 *              {
 *                "_id": 2,
 *                "firstname": "Jane",
 *                "lastname": "Doe",
 *                "email": "jane.doe@domain.com"
 *              }
 *            ]
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist or you are not member of this group.
 * @apiError (422) {Object} WrongMembers Some of the members are not in the group.
 * @apiError (422) {Object} MissingParameters Missing parameters.
 * @apiError (422) {Object} WrongItems Some of the item does not exist or are already checked.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not member of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
payment.addPayment = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;

  Group.findOne({_id: groupId, members: user._id}).exec().then((group) => {
    if (!group) {
      // The group does not exist
      return Promise.reject({
        status: 404,
        message: 'This group does not exist or you are not member of this group.',
      });
    }

    // Group exist
    if (req.body.items && req.body.items.length > 0) {
      // Items related payment
      return checkPaymentItems(req.body.items, group._id);
    } else if (req.body.members && req.body.members.length > 0 && req.body.title && req.body.amount) {
      // Non-items related payment

      for (let member of req.body.members) {
        if (group.members.indexOf(member) < 0) {
          return Promise.reject({
            status: 422,
            message: 'Some of the members are not in the group.',
          });
        }
      }

      return createPaymentItem(req.body.title, user._id, group._id, req.body.members);
    } else {
      // Missing parameters
      return Promise.reject({
        status: 422,
        message: 'Missing parameters.',
      });
    }
  }).then((items) => {
    const newPayment = {
      creator: user._id,
      group: groupId,
      edited: new Date(),
      date: req.body.date ? new Date(req.body.date) : new Date(),
      items: items.map((item) => item._id),
      amount: req.body.amount || 0,
    };
    if (req.body.title) {
      newPayment.title = req.body.title;
    }
    return Payment.create(newPayment);
  }).then((payment) => {
    return payment
      .populate({
        path: 'items',
        select: '_id name creator added group members checked archived payment',
        populate: {
          path: 'members creator',
          select: '_id firstname lastname email',
        },
      })
      .populate('creator', '_id firstname lastname email')
      .execPopulate();
  }).then((payment) => {
    res.status(200).json({
      result: payment,
    });
  }).catch((err) => {
    next(err);
  });
};

/**
 * @api {get} /web-api/v1/groups/:groupId/payments List payments in a group
 * @apiName List payments in a group
 * @apiGroup Payment
 *
 * @apiDescription List payments in a group.
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": [{
 *        "_id": 1,
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "group": 1,
 *        "edited": "2017-05-01T07:30:26.600Z",
 *        "date": "2017-05-01T07:30:26.600Z",
 *        "amount": 324,
 *        "title": "Hotel Booking",
 *        "items": [
 *          {
 *            "_id": 3,
 *            "name": "Hotel Booking",
 *            "creator": {
 *              "_id": 1,
 *              "firstname": "John",
 *              "lastname": "Doe",
 *              "email": "john.doe@domain.com"
 *            },
 *            "added": "2017-05-01T07:30:26.473Z",
 *            "group": 1,
 *            "checked": true,
 *            "members": [
 *              {
 *                "_id": 1,
 *                "firstname": "John",
 *                "lastname": "Doe",
 *                "email": "john.doe@domain.com"
 *              },
 *              {
 *                "_id": 2,
 *                "firstname": "Jane",
 *                "lastname": "Doe",
 *                "email": "jane.doe@domain.com"
 *              }
 *            ]
 *          }
 *        ]
 *      }]
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist or you are not member of this group.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not member of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
payment.listPayments = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;
  let limit = 50;
  if (req.query.limit !== undefined
    && (!isNaN(parseFloat(req.query.limit)) && isFinite(req.query.limit))) {
    limit = parseInt(req.query.limit);
  }

  Group.findOne({_id: groupId, members: user._id}).exec().then((group) => {
    if (!group) {
      // The group does not exist
      return Promise.reject({
        status: 404,
        message: 'This group does not exist or you are not member of this group.',
      });
    }
    return Payment.find({group: groupId})
      .sort({date: -1})
      .limit(limit)
      .populate({
        path: 'items',
        select: '_id name creator added group members checked archived payment',
        populate: {
          path: 'members creator',
          select: '_id firstname lastname email',
        },
      })
      .populate('creator', '_id firstname lastname email')
      .exec();
  }).then((payments) => {
    res.status(200).json({
      result: payments,
    });
  }).catch((err) => {
    next(err);
  });
};

/**
 * @api {put} /web-api/v1/groups/:groupId/payments/:paymentId Edit a payment
 * @apiName Edit a payment
 * @apiGroup Payment
 *
 * @apiDescription Edit a payment.
 *
 * @apiParam {Date} [date] Date of the payment.
 * @apiParam {String} [title] Title of the payment.
 * @apiParam {Number} [amount] Amount of the payment.
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "group": 1,
 *        "edited": "2017-05-01T07:30:26.600Z",
 *        "date": "2017-05-01T07:30:26.600Z",
 *        "amount": 324,
 *        "title": "Hotel Booking",
 *        "items": [
 *          {
 *            "_id": 3,
 *            "name": "Hotel Booking",
 *            "creator": {
 *              "_id": 1,
 *              "firstname": "John",
 *              "lastname": "Doe",
 *              "email": "john.doe@domain.com"
 *            },
 *            "added": "2017-05-01T07:30:26.473Z",
 *            "group": 1,
 *            "checked": true,
 *            "members": [
 *              {
 *                "_id": 1,
 *                "firstname": "John",
 *                "lastname": "Doe",
 *                "email": "john.doe@domain.com"
 *              },
 *              {
 *                "_id": 2,
 *                "firstname": "Jane",
 *                "lastname": "Doe",
 *                "email": "jane.doe@domain.com"
 *              }
 *            ]
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist or you are not member of this group.
 * @apiError (422) {Object} MissingParameters Please provide at least a title, an amount or a date.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not member of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
payment.editPayment = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;
  const paymentId = req.params.paymentId;

  if(!req.body.date && !req.body.amount && !req.body.title) {
    return next({
      status: 422,
      message: 'Please provide at least a title, an amount or a date.',
    });
  }

  Group.findOne({_id: groupId, members: user._id}).exec().then((group) => {
    if (!group) {
      // The group does not exist
      return Promise.reject({
        status: 404,
        message: 'This group does not exist or you are not member of this group.',
      });
    }

    return Payment.findOne({_id: paymentId, group: groupId}).exec();
  }).then((payment) => {
    if(!payment) {
      // The payment does not exist
      return Promise.reject({
        status: 404,
        message: 'This payment does not exist.',
      });
    }

    const update = {};
    if (req.body.date) update.date = req.body.date;
    if (req.body.amount) update.amount = req.body.amount;
    if (req.body.title) update.title = req.body.title;

    // Update the payment
    return Payment.findByIdAndUpdate(paymentId, update, {new: true});
  }).then((payment) => {
    return payment
      .populate({
        path: 'items',
        select: '_id name creator added group members checked archived payment',
        populate: {
          path: 'members creator',
          select: '_id firstname lastname email',
        },
      })
      .populate('creator', '_id firstname lastname email')
      .execPopulate();
  }).then((payment) => {
    res.status(200).json({
      result: payment,
    });
  }).catch((err) => {
    next(err);
  });
};

/**
 * @api {delete} /web-api/v1/groups/:groupId/payments/:paymentId Delete a payment
 * @apiName Delete a payment
 * @apiGroup Payment
 *
 * @apiDescription Delete a payment.
 *
 * @apiParam {Boolean} [keepChecked] Keep the items checked after deletion (default: false).
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "group": 1,
 *        "edited": "2017-05-01T07:30:26.600Z",
 *        "date": "2017-05-01T07:30:26.600Z",
 *        "amount": 324,
 *        "title": "Hotel Booking",
 *        "items": [
 *          {
 *            "_id": 3,
 *            "name": "Hotel Booking",
 *            "creator": {
 *              "_id": 1,
 *              "firstname": "John",
 *              "lastname": "Doe",
 *              "email": "john.doe@domain.com"
 *            },
 *            "added": "2017-05-01T07:30:26.473Z",
 *            "group": 1,
 *            "checked": true,
 *            "members": [
 *              {
 *                "_id": 1,
 *                "firstname": "John",
 *                "lastname": "Doe",
 *                "email": "john.doe@domain.com"
 *              },
 *              {
 *                "_id": 2,
 *                "firstname": "Jane",
 *                "lastname": "Doe",
 *                "email": "jane.doe@domain.com"
 *              }
 *            ]
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist or you are not member of this group.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not member of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
payment.deletePayment = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;
  const paymentId = req.params.paymentId;
  const keepChecked = (req.query.keepChecked !== undefined && req.query.keepChecked !== 'false');

  Group.findOne({_id: groupId, members: user._id}).exec().then((group) => {
    if (!group) {
      // The group does not exist
      return Promise.reject({
        status: 404,
        message: 'This group does not exist or you are not member of this group.',
      });
    }

    return Payment.findOne({_id: paymentId, group: groupId})
      .populate('items', '_id payment')
      .exec();
  }).then((payment) => {
    if (!payment) {
      // The payment does not exist
      return Promise.reject({
        status: 404,
        message: 'This payment does not exist.',
      });
    }

    // If it is a non-item related payment
    if(payment.items[0].payment) {
      // We delete the linked item
      return ShoppingItem.findByIdAndRemove(payment.items[0]._id)
        .exec().then(() => {
          return Promise.resolve(payment);
        });
    }
    // Else, return the payment directly
    return Promise.resolve(payment);
  }).then((payment) => {
    if(!keepChecked) {
      // Uncheck the items
      return ShoppingItem.update(
        {_id: {$in: payment.items}},
        {checked: false},
        {multi: true, new: true}
      ).then((result) => {
        if (result.ok) {
          return Promise.resolve(payment);
        }
      });
    } else {
      return Promise.resolve(payment);
    }
  }).then((payment) => {
    return payment.remove();
  }).then((payment) => {
    return payment
      .populate({
        path: 'items',
        select: '_id name creator added group members checked archived payment',
        populate: {
          path: 'members creator',
          select: '_id firstname lastname email',
        },
      })
      .populate('creator', '_id firstname lastname email')
      .execPopulate();
  }).then((payment) => {
    res.status(200).json({
      result: payment,
    });
  }).catch((err) => {
    next(err);
  });
};

/**
 * @api {get} /web-api/v1/groups/:groupId/balance Get your balance
 * @apiName Get your balance
 * @apiGroup Payment
 *
 * @apiDescription Get your balance.
 *
 * @apiSuccess (200) OK
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": [
 *        {
 *          "user": {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          "balance": 35
 *        },
 *        {
 *          "user": {
 *            "_id": 2,
 *            "firstname": "Jane",
 *            "lastname": "Doe",
 *            "email": "jane.doe@domain.com"
 *          },
 *          "balance": 0
 *        },
 *        {
 *          "user": {
 *            "_id": 3,
 *            "firstname": "Spider",
 *            "lastname": "Man",
 *            "email": "spiderman@hero.com"
 *          },
 *          "balance": 66.33
 *        }
 *      ]
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist or you are not member of this group.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not member of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
payment.balance = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;

  let balances = [];

  Group.findOne({_id: groupId, members: user._id})
    .populate('members', '_id firstname lastname email')
    .exec()
    .then((group) => {
      if (!group) {
        // The group does not exist
        return Promise.reject({
          status: 404,
          message: 'This group does not exist or you are not member of this group.',
        });
      }

      balances = group.members
        .filter((member) => member._id!==user._id)
        .map((member) => {
          return {user: member, balance: 0};
        });

      return Payment.find({group: groupId})
        .where('amount').gt(0)
        .populate('items')
        .exec();
    }).then((payments) => {
      // Filter the payment which belong to me and not me only
      payments = payments.filter((payment) => {
        return (payment.items[0].members.indexOf(user._id) > -1
        || payment.creator == user._id);
      });
      for(let payment of payments) {
        const amount = payment.amount / payment.items[0].members.length;
        // If payment creator
        if(user._id === payment.creator) {
          for(let balance of balances) {
            // If member of the payment, he owes me money
            if(payment.items[0].members.indexOf(balance.user._id) > -1) {
              balance.balance += amount;
            }
          }
        } else {
          for(let balance of balances) {
            // If creator, I own him money
            if(balance.user._id==payment.creator) {
              balance.balance -= amount;
            }
          }
        }
      }

      // Round balances
      balances.map((balance) => {
        balance.balance = Math.round(balance.balance*100)/100;
        return balance;
      });

      res.status(200).json({
        result: balances,
      });
    }).catch((err) => {
      next(err);
    });
};

module.exports = payment;
