'use strict';

// ----- REQUIREMENTS -----

// vendor

// custom
const Group = require('../model/group');
const ShoppingItem = require('../model/shopping-item');

const item = {};

/**
 * @api {get} /web-api/v1/groups/:groupId/items List items in a group
 * @apiName List items in a group
 * @apiGroup Item
 *
 * @apiDescription Get the list of items in a group.
 *
 * @apiParam {Boolean} [checked] Return checked item if true (default: false).
 * @apiParam {Boolean} [archived] Return archived item if true (default: false).
 *
 * @apiSuccess (200) {Object[]} items List of items in the group.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": [
 *        {
 *          "_id": 1,
 *          "name": "Shampoo",
 *          "creator": {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          "added": "2017-03-30T15:30:09.050Z",
 *          "group": 1,
 *          "members": [
 *            {
 *              "_id": 1,
 *              "firstname": "John",
 *              "lastname": "Doe",
 *              "email": "john.doe@domain.com"
 *            }
 *          ]
 *        }
 *      ]
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
item.listItems = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;
  const archived = (req.query.archived !== undefined && req.query.archived !== 'false');
  const checked = (req.query.checked !== undefined && req.query.checked !== 'false');

  Group.find({_id: groupId, members: user._id}).exec()
    .then((group) => {
      if (!group.length > 0) {
        return Promise.reject(
          {status: 404, message: 'This group does not exist.'});
      }

      const query = ShoppingItem.find({group: group[0]._id});
      if(!archived) query.where('archived').equals(false);
      if(!checked) query.where('checked').equals(false);

      return query
        .populate('members', '_id firstname lastname email')
        .populate('creator', '_id firstname lastname email')
        .exec();
    })
    .then((items) => {
      res.status(200).json({result: items});
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {post} /web-api/v1/groups/:groupId/items Add a item in a group
 * @apiName Add an item in a group
 * @apiGroup Item
 *
 * @apiDescription Add an item in a group.
 *
 * @apiHeader {String} Content-Type Must be set to 'application/json'
 *
 * @apiParam {String} name Name of the item to create.
 * @apiParam {Integer[]} members Array of members id.
 *
 * @apiSuccess (200) {Object} item Item just created.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "name": "Item Name",
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "added": "2017-04-25T11:34:00.766Z",
 *        "group": 2,
 *        "members": [
 *          {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          {
 *            "_id": 2,
 *            "firstname": "Jane",
 *            "lastname": "Doe",
 *            "email": "jane.doe@domain.com"
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (422) {Object} MissingParameters Please provide a name and an array of members.
 * @apiError (404) {Object} WrongGroupId This group does not exist.
 * @apiError (422) {Object} MembersError One or more members are not part of the group.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist."
 *       }
 *     }
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "error": {
 *          "status": 422,
 *          "message": "Please provide a name and an array of members."
 *       }
 *     }
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "error": {
 *          "status": 422,
 *          "message": "One or more members are not part of the group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
item.addItem = (req, res, next) => {
  if (!req.body || !req.body.name || req.body.members.length < 1) {
    return next({
      status: 422, message: 'Please provide a name and an array of members.',
    });
  }

  const user = res.locals.user;
  const groupId = req.params.groupId;

  Group.findOne({_id: groupId, members: user._id}).exec()
    .then((group) => {
      if (!group) {
        return Promise.reject(
          {status: 404, message: 'This group does not exist.'});
      }
      for (let member of req.body.members) {
        if (group.members.indexOf(member) < 0) {
          // Member not part of the group
          return Promise.reject({
            status: 403,
            message: 'One or more members are not part of the group.',
          });
        }
      }
    })
    .then(() => {
      // Add item
      const newItem = {
        name: req.body.name,
        creator: user._id,
        added: new Date(),
        group: groupId,
        members: req.body.members,
        checked: false, // By default not checked
        archived: false,  // By default not archived
        payment: false,  // Not payment description
      };
      return ShoppingItem.create(newItem);
    })
    .then((newItem) => {
      return newItem
        .populate('members', '_id firstname lastname email')
        .populate('creator', '_id firstname lastname email')
        .execPopulate();
    })
    .then((newItem) => {
      // Item after population
      return res.status(200).json({
        result: newItem,
      });
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {delete} /web-api/v1/groups/:groupId/items/:itemId Delete a item in a group
 * @apiName Delete an item in a group
 * @apiGroup Item
 * @apiDescription Delete an item in a group, that is not yet checked.
 *
 * @apiHeader {String} Content-Type Must be set to 'application/json'
 *
 * @apiSuccess (200) {Object} item Item just deleted.
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "name": "Item Name",
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "added": "2017-04-25T11:34:00.766Z",
 *        "group": 2,
 *        "members": [
 *          {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          {
 *            "_id": 2,
 *            "firstname": "Jane",
 *            "lastname": "Doe",
 *            "email": "jane.doe@domain.com"
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (422) {Object} WrongParameters The given groupId and the item's group id do not match.
 * @apiError (422) {Object} ItemNotDeletable The element is already checked and may not be deleted.
 * @apiError (404) {Object} WrongGroupId The group for the given groupId does not exist,
 *      or the authenticated user is not part of this group.
 * @apiError (404) {Object} WrongItemId The item for the given itemId does not exist.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not part of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
item.deleteItem = (req, res, next) => {
  const user = res.locals.user;
  const groupId = +req.params.groupId;
  const itemId = +req.params.itemId;

  Group.findOne({_id: groupId, members: user._id}).exec()
    .then((group) => {
      if (!group) {
        return Promise.reject({
          status: 404,
          message: 'This group does not exist or you are not part of this group',
        });
      }
      return ShoppingItem.findById(itemId);
    })
    .then((item) => { // fetch item
      if (!item) { // item does not exist
        return Promise.reject({
          status: 404, message: 'This item does not exist.',
        });
      }
      if (item.checked) { // item does not exist
        return Promise.reject({
          status: 422,
          message: 'The element is already checked and may not be deleted',
        });
      }
      if (item.group !== groupId) {
        // the groupId url parameter doesn't match to the fetched item
        return Promise.reject({
          status: 422,
          message: 'The given group and the item\'s associated group do not match.',
        });
      }
      ShoppingItem.remove({_id: itemId}).exec().then(() => {
        res.send({result: item});
      });
    })
    .catch((err) => {
      next(err);
    });
};


/**
 * @api {patch} /web-api/v1/groups/:groupId/items Update an item in a group
 * @apiName Update an item in a group
 * @apiGroup Item
 *
 * @apiDescription Perform a partial update for a given item in a given group.
 *
 * @apiHeader {String} Content-Type Must be set to 'application/json'
 *
 * @apiParam {String} [name] Name of the item to update.
 * @apiParam {Integer[]} [members] Array of members ids to update.
 *
 * @apiSuccess (200) {Object} item Item just updated.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "name": "Item Name",
 *        "creator": {
 *          "_id": 1,
 *          "firstname": "John",
 *          "lastname": "Doe",
 *          "email": "john.doe@domain.com"
 *        },
 *        "added": "2017-04-25T11:34:00.766Z",
 *        "group": 2,
 *        "members": [
 *          {
 *            "_id": 1,
 *            "firstname": "John",
 *            "lastname": "Doe",
 *            "email": "john.doe@domain.com"
 *          },
 *          {
 *            "_id": 2,
 *            "firstname": "Jane",
 *            "lastname": "Doe",
 *            "email": "jane.doe@domain.com"
 *          }
 *        ]
 *      }
 *    }
 *
 * @apiError (422) {Object} MissingParameters No data for an update was passed.
 * @apiError (422) {Object} WrongParameters One of the parameters does not have
 *    the correct format.
 * @apiError (422) {Object} WrongParameters The given groupId and the item's
 *    group id do not match.
 * @apiError (404) {Object} WrongGroupId The group for the given groupId does
 *    not exist, or the authenticated user is not part of this group.
 * @apiError (404) {Object} WrongItemId The item for the given itemId does not
 *    exist.
 * @apiError (403) {Object} MembersError One or more members are not part of the
 *    group.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 422 Unprocessable Entity
 *     {
 *       "error": {
 *          "status": 422,
 *          "message": "The members field must be a array of numbers, that is
 *            not empty."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
item.updateItem = (req, res, next) => {
  const user = res.locals.user;
  const groupId = +req.params.groupId;
  const itemId = +req.params.itemId;
  const body = req.body;

  const itemPatch = {}; // object that hols the update values

  if (!body || (!body.name && req.members)) { // check if any data was passed
    return Promise.reject({
      status: 422, message: 'No data provided to update the item.',
    });
  }
  Group.findOne({_id: groupId, members: user._id}).exec()
    .then((group) => {
      if (!group) {
        return Promise.reject({
          status: 404,
          message: 'This group does not exist or you are not part of this group',
        });
      }
      if (body.members) { // validate new members if members were passed
        if (!Array.isArray(body.members) || body.members.length < 1) {
          return Promise.reject({
            status: 422,
            message: 'The members field must be a array of numbers, that is not empty.',
          });
        }
        // check if members are part of the group
        for (const itemMember of body.members) {
          if (group.members.indexOf(itemMember) < 0) {
            return Promise.reject({
              status: 422,
              message: 'One or more members are not part of the group.',
            });
          }
        }
        itemPatch.members = body.members;
      }
      if (body.name) { // validate new name if a name was passed
        if (!body.name.trim() || typeof body.name !== 'string') {
          return Promise.reject({
            status: 422, message: 'The name cannot be empty.',
          });
        }
        itemPatch.name = body.name;
      }
      return ShoppingItem.findById(itemId);
    })
    .then((item) => { // fetch item
      if (!item) { // item does not exist
        return Promise.reject({
          status: 404, message: 'This item does not exist.',
        });
      }
      if (item.group !== groupId) {
        // the groupId url parameter doesn't match to the fetched item
        return Promise.reject({
          status: 422,
          message: 'The given group and the item\'s associated group do not match.',
        });
      }
      return ShoppingItem.findByIdAndUpdate(
        itemId, {$set: itemPatch}, {new: true});
    })
    .then((item) => {
      return item
        .populate('members', '_id firstname lastname email')
        .populate('creator', '_id firstname lastname email')
        .execPopulate();
    }).then((item) => {
      res.send({result: item});
    })
    .catch((err) => {
      next(err);
    });
};

/**
 * @api {post} /web-api/v1/groups/:groupId/items/:itemId/archive Archive an item in a group
 * @apiName Archive an item in a group.
 * @apiGroup Item
 *
 * @apiDescription Archive the given item in a given group.
 *
 * @apiSuccess (200) {Object} item Item just updated.
 *
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 200 OK
 *    {
 *      "result": {
 *        "_id": 1,
 *        "name": "Name of the item",
 *        "creator": 1,
 *        "added": "2017-04-29T12:56:43.260Z",
 *        "group": 1,
 *        "archived": true,
 *        "checked": true,
 *        "members": [
 *          1
 *        ]
 *      }
 *    }
 *
 * @apiError (404) {Object} WrongGroupId This group does not exist or you are not part of this group.
 * @apiError (404) {Object} WrongItemId This item does not exist or is not part of this group.
 * @apiError (422) {Object} AlreadyArchived This item is already archived.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 404 Not Found
 *     {
 *       "error": {
 *          "status": 404,
 *          "message": "This group does not exist or you are not part of this group."
 *       }
 *     }
 *
 * @apiUse CheckToken
 * @apiUse ServerTimeout
 */
item.archiveItem = (req, res, next) => {
  const user = res.locals.user;
  const groupId = req.params.groupId;
  const itemId = req.params.itemId;

  Group.findOne({_id: groupId, members: user._id}).exec()
    .then((group) => {
      if (!group) {
        return Promise.reject({
          status: 404,
          message: 'This group does not exist or you are not part of this group',
        });
      }

      return ShoppingItem.findOne({_id: itemId, group: groupId});
    }).then((item) => { // fetch item
      if (!item) { // item does not exist
        return Promise.reject({
          status: 404,
          message: 'This item does not exist or is not part of this group.',
        });
      }
      if(item.archived) {
        return Promise.reject({
          status: 422,
          message: 'This item is already archived.',
        });
      }

      return ShoppingItem.findByIdAndUpdate(itemId, {archived: true}, {new: true});
    }).then((item) => {
      res.status(200).json({
        result: item,
      });
    }).catch((err) => {
      next(err);
    });
};

module.exports = item;
