'use strict';

// ----- REQUIREMENTS -----

// vendor

// custom
const User = require('../model/user');
const AccessToken = require('../model/access-token');

const auth = {};

/**
 * @api {post} /web-api/v1/authenticate Request an access token
 * @apiName Authenticate
 * @apiGroup Auth
 *
 * @apiDescription Request an access token.
 *
 * There is two way to get an access token:
 *
 * The first one is to give your email and password.
 *
 * It is also possible to obtain a new token by sending a token which
 *    has not expired yet.
 *
 * @apiParam {String} [email] User email address.
 * @apiParam {String} [password] User password.
 * @apiParam {String} [accessToken] Access token to renew.
 *
 * @apiSuccess (200) {Object} accessToken Access token valid for an hour
 *    containing user information.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *      "token": "0zqnzx6jdsh1jiheuotaxrms4ihz1vurd6pws82479sbgldi",
 *      "expire": "2017-04-25T09:57:25.157Z",
 *      "user": {
 *        "_id": 1,
 *        "firstname": "John",
 *        "lastname": "Doe",
 *        "email": "john.doe@domain.com"
 *      }
 *    }
 *
 * @apiError (422) {Object} MissingParameters Please provide the user email
 *    and password or the access token to renew.
 * @apiError (403) {Object} TokenInvalid Failed to authenticate token.
 * @apiError (403) {Object} UserNotFound Authentication failed. User not found.
 * @apiError (403) {Object} WrongPassword Authentication failed. Wrong password.
 * @apiError (403) {Object} AccountNotActivated Authentication failed.
 *    The user hasn't activated his account yet.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "error": {
 *          "status": 403,
 *          "message": "Authentication failed. User not found."
 *       }
 *     }
 *
 * @apiUse ServerTimeout
 */
auth.authenticate = (req, res, next) => {
  if (!req.body
    || (!req.body.email && !req.body.password && !req.body.token)
    || (req.body.email && !req.body.password && !req.body.token)
    || (req.body.password && !req.body.email && !req.body.token)) {
    return next({
      status: 422,
      message: 'Please provide the user email and password or the access token to renew.',
    });
  }

  if (req.body.token) {
    // Renew the token
    AccessToken.isValid(req.body.token)
      .then((token) => {
        return User.findById(token.user); // Token valid
      }, (err) => {
        return Promise.reject({status: 403, message: err}); // Token invalid
      })
      .then((user) => {
        if (!user) { // this should not happen
          return Promise.reject('The user for the given token does not exist');
        }
        // Remove old token
        return AccessToken.remove({user: user._id, type: 'API'})
          .then(() => {
            // Create a new token (expire in 1h)
            return AccessToken.createToken(60, 'API', user._id);
          })
          .then((newToken) => {
            // Token creation success
            // Return the information including token as JSON
            newToken = newToken.toObject();
            user = user.toObject();
            delete user.passwordHash;
            delete newToken._id;
            delete newToken.type;
            newToken.user = user;
            return res.status(200).json({result: newToken});
          });
      })
      .catch((err) => {
        next(err);
      });
  } else {
    // Create a new token
    User.findOne({email: req.body.email})
      .then((user) => {
        if (!user) {
          // User not found
          return Promise.reject({
            status: 403,
            message: 'Authentication failed. User not found.',
          });
        }
        if (!user.passwordHash) {
          return Promise.reject({
            status: 403,
            message: 'Your account has not been activated yet.',
          });
        }
        // User found -> check if password matches
        if (!User.validPassword(req.body.password, user.passwordHash)) {
          // Password invalid
          return Promise.reject({
            status: 403,
            message: 'Authentication failed. Wrong password.',
          });
        }
        // If user is found and password is right
        // Remove old token if exist
        return AccessToken.remove({user: user._id, type: 'API'})
          .then(() => {
            // Create a new token - expires in 1h
            return AccessToken.createToken(60, 'API', user._id);
          })
          .then((newToken) => {
            // Token creation success
            // Return the information including token as JSON
            newToken = newToken.toObject();
            user = user.toObject();
            delete user.passwordHash;
            delete newToken._id;
            delete newToken.type;
            newToken.user = user;
            return res.status(200).json({result: newToken});
          });
      })
      .catch((err) => {
        next(err);
      });
  }
};

/**
 * @apiDefine CheckToken
 *
 * @apiHeader {String} [x-access-token] User access token.
 *
 * @apiParam {String} [token] User access token.
 *
 * @apiError (401) {Object} NoTokenProvided No token provided.
 * @apiError (403) {Object} TokenInvalid Failed to authenticate token.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 403 Forbidden
 *     {
 *       "error": {
 *          "status": 403,
 *          "message": "Failed to authenticate token."
 *       }
 *     }
 */
auth.checkToken = (req, res, next) => {
  // Check header or url parameters or post parameters for token
  const token = req.body.token || req.query.token
    || req.headers['x-access-token'];

  if (token) {
    // Check token validity
    AccessToken.isValid(token).then((token) => {
      // Token valid
      return User.findById(token.user, '_id firstname lastname email').exec();
    }, (err) => {
      // Token not valid
      return next({
        status: 403,
        message: 'Failed to authenticate token.',
      });
    }).then((user) => {
      res.locals.user = user;
      next();
    }, (err) => {
      return console.log(err);
    });
  } else {
    // If there is no token return an error
    return next({
      status: 401,
      message: 'No token provided.',
    });
  }
};

module.exports = auth;
