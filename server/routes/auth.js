'use strict';

// ----- LOAD ENV VARIABLES -----

require('dotenv').config();

// ----- REQUIREMENTS -----

// vendor
const express = require('express');
const bodyParser = require('body-parser');

// custom
const User = require('../model/user');
const AccessToken = require('../model/access-token');
const NodeMailer = require('../modules/mailer');

const router = new express.Router();

const tokenTypeName = 'AccountValidation';

// ----- MIDDLEWARES -----

router.use(bodyParser.urlencoded({extended: false}));
router.use(bodyParser.json());

// ----- ROUTES -----

router.get('/signup', (req, res) => {
  res.render('signup', {
    title: 'Sign Up',
  });
});

router.post('/signup', (req, res) => {
  let newUser = req.body;

  if (!newUser.firstname || !newUser.lastname || !newUser.email) {
    return res.render('signup', {
      title: 'Sign Up',
      error: 'Please fill in every input field.',
    });
  }
  User.findByEmail(newUser.email)
    .then((user) => {
      if (!user) {
        // user does not exist -> continue
        return Promise.resolve();
      }
      // user exists already -> check token
      return AccessToken.findOne({user: user._id}).then((token) => {
        if (token
          && token.type === tokenTypeName
          && token.expire < new Date()) {
          // user did not activate his account -> delete token and ser
          return AccessToken.remove({_id: token._id}).then(() => {
            return User.remove({_id: user._id});
          });
        } else {
          return Promise.reject(
            'This email address already exist'); // -> catch block
        }
      });
    })
    .then((deleted) => {
      if (deleted) {
        console.log('Deleted unactivated user account ' + newUser.email);
      }
      return User.create(newUser);
    })
    .then((newUser) => {
      if (!newUser) { // User creation failed
        return Promise.reject('User creation failed. Please try again later.');
      }
      return AccessToken.createToken(
        43200, // expire in 43200 min (30 days)
        tokenTypeName,
        newUser
      ).then((newToken) => {
        // Token creation success
        let recipients = [newUser.email];
        return res.render('email/user-confirmation', {
          title: 'Shared Shopping List - Activation email',
          user: newUser,
          activationLink: `${req.protocol}://${req.get('host')}/user/activate/${newToken.token}`,
        }, (err, html) => {
          if (err) { // Rendring failed
            console.error(err);
            return Promise.reject(
              'Your account has been created, but something went wrong. ' +
              'Our support will get in touch with you soon.');
          }
          return NodeMailer.sendEmail(
            recipients, 'Confirm your account on Shared Shopping List', html);
        });
      });
    })
    .then((message) => {
      // Email success
      console.log('Email sent.');
      return res.redirect(301, '/signup/confirmation');
    })
    .catch((err) => {
      console.log(err);
      return res.render('signup', {
        title: 'Sign up',
        error: err,
      });
    });
});


router.get('/signup/confirmation', (req, res) => {
  res.render('signup-confirmation', {
    title: 'User account created',
  });
});

router.get('/user/activate/:accessToken', (req, res) => {
  const accessToken = req.params.accessToken;

  AccessToken.find()
    .where('token').equals(accessToken)
    .where('type').equals(tokenTypeName)
    .where('expire').gte(new Date())
    .exec()
    .then((token) => {
      if (token.length >= 1) {
        // Activation token found
        res.render('user-activation', {
          title: 'Activate your account',
          formAction: req.protocol + '://' + req.get('host') + req.originalUrl,
        });
      } else {
        // Activation token not valid
        res.render('user-activation', {
          expired: true,
          title: 'Account activation failed',
        });
      }
    }).catch((err) => {
    // Error querying the DB
    res.render('user-activation', {
      expired: true,
      title: 'Account activation failed',
    });
  });
});

router.post('/user/activate/:accessToken', (req, res) => {
  const accessToken = req.params.accessToken;
  if (req.body.password !== req.body.passwordCheck) {
    // Passwords does not match
    return res.render('user-activation', {
      title: 'Activate your account',
      formAction: req.protocol + '://' + req.get('host') + req.originalUrl,
      error: 'The two passwords must be equal.',
    });
  } else if (req.body.password.length < 8) {
    // Password length < 8
    return res.render('user-activation', {
      title: 'Activate your account',
      formAction: req.protocol + '://' + req.get('host') + req.originalUrl,
      error: 'The password must contain at least 8 characters.',
    });
  } else {
    // Password valid
    AccessToken.findOne({token: accessToken}).exec().then((token) => {
      // Token found
      User.setPassword(token.user, req.body.password).then((user) => {
        // User password set

        // Remove activation token
        AccessToken.remove(token, (err) => {
          if (err) {
            console.log('Error removing the access token.');
          }
        });

        return res.render('user-confirmation', {
          title: 'Account activated',
        });
      }).catch((err) => {
        // Error setting the password
        console.log(err);
      });
    }).catch((err) => {
      // Error querying the token
      console.log(err);
    });
  }
});

module.exports = router;
