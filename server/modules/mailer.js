'use strict';

// ----- LOAD ENV VARIABLES -----

require('dotenv').config();

// ----- REQUIREMENTS -----

// vendor
const nodeMailer = require('nodemailer');

// ----- SMTP CONFIG -----

const smtpConfig = {
  pool: true,
  host: 'smtp.gmail.com',
  port: 465,
  secure: true, // use TLS
  auth: {
    user: process.env.GMAIL_ADDRESS,
    pass: process.env.GMAIL_PASSWORD,
  },
  tls: {
    // do not fail on invalid certificates
    rejectUnauthorized: false,
  },
};

// ----- NODE MAILER -----

class NodeMailer {
  constructor() {
    this.transporter = nodeMailer.createTransport(smtpConfig);

    this.mailOptions = {
      from: '"SSSF Project" <sssfproject@gmail.com>', // sender address
    };
  }

  sendEmail(recipients, subject, html) {
    return new Promise((resolve, reject) => {
      this.mailOptions.to = recipients.toString();
      this.mailOptions.subject = subject;
      this.mailOptions.html = html;

      // send mail with defined transport object
      this.transporter.sendMail(this.mailOptions, (error, info) => {
        if (error) {
          reject('Error sending the email');
        }
        resolve('Email sent');
      });
    });
  }
}

module.exports = new NodeMailer();
