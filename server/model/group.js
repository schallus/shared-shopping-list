'use strict';

// ----- REQUIREMENTS -----

// vendor
const jimp = require("jimp");
const fs = require('fs');

// custom
const DB = require('../modules/database');

// ----- MODEL -----

const group = {
  name: {type: String},
  creator: {type: Number, ref: 'User', index: true},
  members: [
    {type: Number, ref: 'User', index: true},
  ],
  image: {type: String},
  thumbnail: {type: String},
};

const groupSchema = DB.getSchema(group, 'Group');

// ----- STATIC METHODS -----

groupSchema.methods.createThumbnail = function(imageFile, width, height) {
  return new Promise((resolve, reject) => {

    // Create a new thumbnail
    const destFileName = `${width}x${height}_${imageFile.filename}`;
    const publicFolderUrl = '/uploads/img';
    jimp.read(imageFile.path).then((image) => {
      image.cover(width, height)
        .quality(60)
        // .greyscale()
        .write(`${imageFile.destination}/${destFileName}`);

      // Delete old thumbnail if exist
      if(this.thumbnail) {
        const thumbnailPath = __dirname + '/../../public' + this.thumbnail;
        fs.unlink(thumbnailPath, (err) => {
          if (err) {
            return console.log(err);
          }
          // successfully deleted the old thumbnail
        });
      }
      // Delete old original image if exist
      if(this.image) {
        const thumbnailPath = __dirname + '/../../public' + this.image;
        fs.unlink(thumbnailPath, (err) => {
          if (err) {
            return console.log(err);
          }
          // successfully deleted the old image
        });
      }

      // Update group thumbnail
      this.update({
        thumbnail: `${publicFolderUrl}/${destFileName}`,
        image: `${publicFolderUrl}/${imageFile.filename}`,
      }).exec().then(() => {
        this.thumbnail = `${publicFolderUrl}/${destFileName}`;
        this.image = `${publicFolderUrl}/${imageFile.filename}`;
        resolve(this);
      }, (err) => {
        console.log(err);
        return reject('Error updating the group');
      });
    }).catch((err) => {
      console.log(err);
      return reject('Error creating the thumbnail');
    });
  });
};

// ----- EXPORT -----

module.exports = DB.getModel(groupSchema, 'Group');
