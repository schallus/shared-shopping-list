'use strict';

// ----- GLOBAL VARIABLES -----

const saltRounds = 10;

// ----- REQUIREMENTS -----

// custom
const DB = require('../modules/database');

// ----- MODEL -----

const accessToken = {
  token: {type: String},
  expire: {type: Date},
  type: {type: String, enum: ['AccountValidation', 'API']},
  user: {type: Number, ref: 'User', index: true},
};

const accessTokenSchema = DB.getSchema(accessToken, 'AccessToken');

// ----- METHODS -----

accessTokenSchema.statics.createToken = (minutes, type, userId) => {
  return new Promise((resolve, reject) => {
    const token = {
      token: Math.random().toString(36).substr(2)
      + Math.random().toString(36).substr(2),
      expire: new Date().setMinutes(new Date().getMinutes() + minutes),
      type: type,
      user: userId,
    };

    DB.getModel(accessTokenSchema, 'AccessToken').create(token, (err, newToken) => {
      if(err) {
        reject('Error creating the token');
      }
      resolve(newToken);
    });
  });
};

accessTokenSchema.statics.isValid = (token) => {
  return new Promise((resolve, reject) => {
    DB.getModel(accessTokenSchema, 'AccessToken').findOne({token: token}, (err, token) => {
      if(token && new Date() <= token.expire) {
        resolve(token);
      } else {
        reject('Access token not valid.');
      }
    });
  });
};

// ----- EXPORT -----

module.exports = DB.getModel(accessTokenSchema, 'AccessToken');
