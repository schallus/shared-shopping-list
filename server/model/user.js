'use strict';

// ----- GLOBAL VARIABLES -----

const saltRounds = 10;

// ----- REQUIREMENTS -----

// vendor
const bcrypt = require('bcrypt');
// custom
const DB = require('../modules/database');

// ----- MODEL -----

const user = {
  firstname: {type: String},
  lastname: {type: String},
  email: {type: String},
  passwordHash: {type: String},
};

const userSchema = DB.getSchema(user, 'User');

// ----- STATIC METHODS -----

userSchema.statics.validPassword = (plaintextPassword, passwordHash) => {
  return bcrypt.compareSync(plaintextPassword, passwordHash);
};

userSchema.statics.findByEmail = (email) => {
  return DB.getModel(userSchema, 'User').findOne({email: email});
};

userSchema.statics.setPassword = (userId, password) => {
  const passwordHash = bcrypt.hashSync(password, saltRounds);
  return DB.getModel(userSchema, 'User').findByIdAndUpdate(userId, {passwordHash: passwordHash}, {new: true});
};

userSchema.statics.exist = (userId) => {
  return new Promise((resolve, reject) => {
    DB.getModel(userSchema, 'User').findById(userId, (err, user) => {
      if(user) {
        resolve();
      } else {
        reject(`User ${userId} does not exist.`);
      }
    });
  });
};

// ----- EXPORT -----

module.exports = DB.getModel(userSchema, 'User');
