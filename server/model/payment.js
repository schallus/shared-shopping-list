'use strict';

// ----- REQUIREMENTS -----

// custom
const DB = require('../modules/database');

// ----- MODEL -----

const payment = {
  title: {type: String},
  creator: {type: Number, ref: 'User', index: true},
  group: {type: Number, ref: 'Group', index: true},
  edited: {type: Date},
  date: {type: Date},
  items: [
    {type: Number, ref: 'ShoppingItem', index: true},
  ],
  amount: {type: Number},
};

const paymentSchema = DB.getSchema(payment, 'Payment');

// ----- EXPORT -----

module.exports = DB.getModel(paymentSchema, 'Payment');
