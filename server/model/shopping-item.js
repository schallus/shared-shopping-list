'use strict';

// ----- REQUIREMENTS -----

// custom
const DB = require('../modules/database');

// ----- MODEL -----

const shoppingItem = {
  name: {type: String},
  creator: {type: Number, ref: 'User', index: true},
  added: {type: Date},
  group: {type: Number, ref: 'Group', index: true},
  members: [
    {type: Number, ref: 'User', index: true},
  ],
  checked: {type: Boolean, default: false},
  archived: {type: Boolean, default: false},
  payment: {type: Boolean, default: false},
};

const shoppingItemSchema = DB.getSchema(shoppingItem, 'ShoppingItem');

// ----- EXPORT -----

module.exports = DB.getModel(shoppingItemSchema, 'ShoppingItem');
