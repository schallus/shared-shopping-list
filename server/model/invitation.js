'use strict';

// ----- REQUIREMENTS -----

// custom
const DB = require('../modules/database');

// ----- MODEL -----

const invitation = {
  date: {type: Date},
  sender: {type: Number, ref: 'User', index: true},
  user: {type: Number, ref: 'User', index: true},
  group: {type: Number, ref: 'Group', index: true},
};

const invitationSchema = DB.getSchema(invitation, 'Invitation');

// ----- EXPORT -----

module.exports = DB.getModel(invitationSchema, 'Invitation');
