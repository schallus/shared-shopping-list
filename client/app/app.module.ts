import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastModule, ToastOptions} from 'ng2-toastr/ng2-toastr';

import {AppComponent} from './app.component';
import {MenuComponent} from './menu.component';
import {PageNotFoundComponent} from './not-found.component';

import {AppRoutingModule} from './app-routing.module';
import {ShoppingListsModule} from './shopping-lists/shopping-lists.module';

import {WalletComponent} from './wallet/wallet.component';
import {SharedModule} from './shared/shared.module';
import {WalletModule} from './wallet/wallet.module';

// toastr config
export class CustomToastrOptions extends ToastOptions {
  animate = 'flyLeft'; // you can override any options available
  showCloseButton = true;
  toastLife = 5000;
  positionClass = 'toast-bottom-left ';
  maxShown = 7;
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    ToastModule.forRoot(),
    SharedModule,
    ShoppingListsModule,
    WalletModule,
    AppRoutingModule,
  ],
  declarations: [
    AppComponent,
    MenuComponent,
    PageNotFoundComponent,
  ],
  providers: [
    {provide: ToastOptions, useClass: CustomToastrOptions},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
