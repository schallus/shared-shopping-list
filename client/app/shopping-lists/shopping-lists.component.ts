import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
import {ActivatedRoute, Router} from '@angular/router';
import {EditableShoppingItem, NewShoppingItem, ShoppingItem, ShoppingListService} from './shopping-list.service';
import {Group, GroupService} from '../shared/groups/group.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/forkJoin';
import {AuthService, User} from '../shared/account/auth.service';
import {NewPayment, PaymentService} from '../wallet/wallet.service';

export class ItemList {
  name: string;
  criterion: (ShoppingItem) => boolean;
  newItem: NewShoppingItem;
  items: ShoppingItem[];
  showMembers: boolean;
  addMethod: number;
}


@Component({
  templateUrl: './shopping-lists.component.html',
})
export class ShoppingListsComponent implements OnInit {

  // ----- VIEW REFERENCES -----
  @ViewChild('shoppingItemModal') itemModal: ElementRef;
  modal: NgbModalRef;

  // ----- EDIT AND CREATE VARIABLES -----
  // Models
  newItem: NewShoppingItem = new NewShoppingItem();
  editItem: NewShoppingItem;
  selectedItems: ShoppingItem[] = [];
  price: number;

  // Util
  editMode = false; // initial value
  editRequestRunning = false;
  paymentRequestRunning = false;
  itemsMapping: { [k: string]: string } = {'=1': '# item', 'other': '# items'};
  showChecked = false;
  addMethods = {
    all: 10,
    me: 20,
  };

  // ----- FETCHED DATA -----
  group: Group;
  groupMembers: number[]; // precomputed array of the members's primary keys (for performance reasons)
  checkedItems: ShoppingItem[];

  lists: ItemList[] = [
    {
      name: 'Personal',
      criterion: (item: ShoppingItem) => item.members.length === 1 && item.members[0]._id === this.authService.user._id,
      items: [],
      newItem: new NewShoppingItem(),
      showMembers: false,
      addMethod: this.addMethods.me,
    },
    {
      name: 'Common',
      criterion: (item: ShoppingItem) => item.members.length > 1,
      items: [],
      newItem: new NewShoppingItem(),
      showMembers: true,
      addMethod: this.addMethods.all,
    },
    {
      name: 'Others',
      criterion: (item: ShoppingItem) => item.members.length === 1 && item.members[0]._id !== this.authService.user._id,
      items: [],
      newItem: null,
      showMembers: true,
      addMethod: null,
    }
  ];

  constructor(private groupService: GroupService,
              private itemService: ShoppingListService,
              private authService: AuthService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private toastr: ToastsManager,
              private paymentService: PaymentService,
              private router: Router,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.resetSelection();
    this.route.params
      .map((params) => +params['id'])
      .skipWhile((id) => isNaN(id))
      // fetch group and items
      .switchMap((groupID) => {
        const items = this.itemService.getItems(groupID, true);
        const group = this.groupService.getGroup(groupID);
        return Observable.forkJoin([items, group]);
      })
      .subscribe((results) => {
        // set group
        this.group = results[1];
        // extract array of member ids
        this.groupMembers = this.group.members.map((m) => m._id);
        // set all members as default for new item
        this.resetNewItem();
        // refresh items
        this.displayItems(results[0]);
      }, (err) => {
        this.handleError(err);
      });
  }

  // --------- CRUD operations ---------

  addItemToList(list: ItemList) {
    switch (list.addMethod) {
      case this.addMethods.all:
        list.newItem.members = this.groupMembers;
        break;
      case this.addMethods.me:
        list.newItem.members = [this.authService.user._id];
        break;
      default:
        console.error('No method for adding item provided.');
    }
    this.itemService.createItem(this.group._id, list.newItem)
      .subscribe((item) => {
        this.toastr.success(`Added ${item.name} to ${list.name.toLowerCase()} items`);
        list.newItem = new NewShoppingItem();
        this.displayItem(item);
      }, (err) => {
        this.handleError(err);
      });
  }

  addOrUpdateItem(item: NewShoppingItem) {
    this.editRequestRunning = true;
    const groupID = this.group._id;
    new Promise((resolve, reject) => {
      if (item.members.length === 0) {
        return reject('A item must have at least one member.');
      }
      // Update
      if (this.itemIsEditable(item)) {
        const editableItem = <EditableShoppingItem> item;
        if (item.name.length === 0) {
          return reject('The item title is required.');
        }
        this.itemService.updateItem(groupID, editableItem)
          .subscribe((updatedItem) => {
            this.toastr.success(`Updated ${updatedItem.name}`);
            if (!this.displayUpdate(updatedItem)) {
              // updating existing item failed
              console.error('Updated not existing item.');
              this.displayItem(updatedItem);
            }
            this.closeModal();
          }, (err) => {
            this.handleError(err);
          }, () => {
            resolve();
          });
        // Add
      } else {
        const titleArray = item.name.split(',');
        // filter duplicates and empty values
        const itemTitles = titleArray.map((title) => title.trim())
          .filter((title, index, titles) => !!title && titles.indexOf(title) === index);
        let infoMsg = `Trying to add ${itemTitles.length === 1 ? '1 item' : itemTitles.length + ' items'}.`;
        const skppedTitles = titleArray.length - itemTitles.length;
        infoMsg += skppedTitles > 0 ? ` Skipped ${skppedTitles} titles` : '';
        this.toastr.info(infoMsg);
        if (itemTitles.length === 0) {
          return reject('Please provide at least one item title.');
        }
        const obsevArray = itemTitles.map((title) => this.itemService
          .createItem(groupID, {name: title, members: item.members}));
        Observable.merge(obsevArray).mergeMap((val) => val)
          .subscribe((createdItem) => {
            this.toastr.success(`Added ${createdItem.name}.`);
            this.displayItem(createdItem);
            this.closeModal();
          }, (err) => {
            this.handleError(err);
          }, () => {
            resolve();
          });
      }
    }).then((msg: string) => {
      this.editRequestRunning = false;
      this.resetNewItem();
    }).catch((err) => {
      this.toastr.error(err);
      this.editRequestRunning = false;
    });
  }

  private deleteItem(item: EditableShoppingItem) {
    this.editRequestRunning = true;
    this.itemService.deleteItem(this.group._id, item._id)
      .subscribe((deletedIem) => {
        this.editRequestRunning = false;
        this.toastr.info(`Deleted ${deletedIem.name}`);
        if (!this.hideItem(deletedIem)) {
          // updating existing item failed
          console.log('Item was already removed from view');
        }
        this.closeModal();
      }, (err) => {
        this.editRequestRunning = false;
        this.handleError(err);
      });
  }

  private checkItems() {
    this.paymentRequestRunning = true;
    const groupID = this.group._id;
    if (this.selectedItems.length === 0) {
      return this.toastr.error('Please select at least one item');
    }
    const newPayment: NewPayment = {
      title: '',
      items: this.selectedItems.map((item) => item._id),
      amount: this.price,
      date: new Date(),
      members: null,
    };
    this.paymentService.createPayment(groupID, newPayment).subscribe(
      (payment) => {
        this.paymentRequestRunning = false;
        this.toastr.success(
          `Checked ${payment.items.length === 1 ? '1 item' : payment.items.length + ' items'}.`);
        payment.items.forEach((item) => this.hideItem(item));
        this.resetSelection();
        payment.items.forEach((i) => this.checkedItems.push(i));
      },
      (err) => {
        this.paymentRequestRunning = false;
        this.handleError(err);
      }
    );
  }

  // --------- UI HELPER FUNCTIONS ---------

  private displayItems(items: ShoppingItem[]) {
    this.checkedItems = items.filter(item => item.checked);
    const uncheckedItems = items.filter(item => !item.checked);
    for (const list of this.lists) {
      list.items = uncheckedItems.filter(list.criterion);
    }
  }

  private displayItem(item: ShoppingItem) {
    for (const list of this.lists) {
      if (list.criterion(item)) {
        list.items.push(item);
      }
    }
  }

  private displayUpdate(updatedItem: ShoppingItem): boolean {
    for (const list of this.lists) {
      for (let i = 0; i < list.items.length; i++) {
        if (list.items[i]._id === updatedItem._id) {
          if (list.criterion(updatedItem)) {
            list.items[i] = updatedItem;
          } else {
            list.items.splice(i, 1);
            this.displayItem(updatedItem);
          }
          return true;
        }
      }
    }
    return false;
  }

  private hideItem(updatedItem: ShoppingItem): boolean {
    for (const list of this.lists) {
      for (let i = 0; i < list.items.length; i++) {
        if (list.items[i]._id === updatedItem._id) {
          list.items.splice(i, 1);
          return true;
        }
      }
    }
    return false;
  }

  private hideCheckedItem(item): boolean {
    for (let i = 0; i < this.checkedItems.length; i++) {
      if (this.checkedItems[i]._id === item._id) {
        this.checkedItems.splice(i, 1);
        return true;
      }
    }
    return false;
  }


  private openModal(content) {
    this.modal = this.modalService.open(content);
  }

  private closeModal() {
    if (this.modal) {
      this.modal.close();
    }
  }

  // --------- ACTION METHODS ---------

  onItemClicked(item: ShoppingItem) {
    if (this.editMode) {
      const editableItem = new EditableShoppingItem(item);
      this.showEditDialog(editableItem);
    } else {
      this.selectItem(item);
    }
  }

  showEditDialog(item: NewShoppingItem) {
    this.editItem = item;
    this.openModal(this.itemModal);
  }

  toogleEditMode() {
    if (this.editMode) {
      this.editMode = false;
    } else {
      this.editMode = true;
      this.resetSelection();
    }
  }

  refreshItems() {
    this.itemService.getItems(this.group._id, true).subscribe(items => {
      this.displayItems(items);
      this.toastr.info('Refreshed items');
    }, err => {
      this.handleError(err);
    });
  }

  onCheckedItemClicked(item: ShoppingItem, event) {
    event.stopPropagation();
    this.router.navigate(['/wallet/group/', this.group._id]);
  }

  archiveItem(item: ShoppingItem) {
    this.itemService.archiveItem(this.group._id, item).subscribe(
      (updatedItem) => {
        this.toastr.info('Archived ' + updatedItem.name);
        this.hideCheckedItem(item);
      }, (err) => {
        this.handleError(err);
      }
    );
  }

  // --------- HELPER FUNCTIONS ---------

  private resetNewItem() {
    this.newItem = {
      name: '',
      members: [...this.groupMembers], // copy groupMembers
    };
  }

  private resetSelection() {
    this.price = null;
    this.selectedItems = [];
  }

  private selectItem(item: ShoppingItem) {
    const index = this.selectedItems.indexOf(item, 0);
    if (index > -1) {
      this.selectedItems.splice(index, 1);
    } else {
      if (this.itemIsSelectable(item)) {
        this.selectedItems.push(item);
      } else {
        this.toastr.info('You can only select items with the same ' +
          'participants at once, cause the price will be divided equally.');
      }
    }
  }

  toggleShowChecked() {
    this.showChecked = !this.showChecked;
  }

  // --------- BOOLEAN FUNCTIONS ---------

  private itemIsSelectable(item: ShoppingItem) {
    if (this.selectedItems.length > 0) {
      const selectedItem = this.selectedItems[0];
      if (selectedItem.members.length === item.members.length) {
        for (const member of item.members) {
          if (!selectedItem.members.find((m) => m._id === member._id)) {
            return false;
          }
        }
      } else {
        return false;
      }
    }
    return true;
  }

  private itemIsSelected(item: ShoppingItem): boolean {
    return this.selectedItems.indexOf(item) > -1;
  }

  private itemIsEditable(item): boolean {
    return !!item._id;
  }

  // --------- ERROR HANDLER ---------

  private handleError(err) {
    if (err.status === 404) {
      this.toastr.error(err.message);
      // this.router.navigate(['/']);
    } else {
      this.toastr.error(err.message);
    }
  }
}
