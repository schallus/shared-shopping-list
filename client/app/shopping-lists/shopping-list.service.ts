import {Injectable} from '@angular/core';
import {Http, URLSearchParams} from '@angular/http';
import {Member} from '../shared/groups/group.service';
import {Observable} from 'rxjs/Observable';
import {ApiHelper} from '../shared/api-helper/api-helper.service';
import {AuthService} from '../shared/account/auth.service';

export class ShoppingItem {
  _id: number;
  name: string;
  members: Member[];
  checked: boolean;
}

export class NewShoppingItem {
  name: string;
  members: number[];
}

/** Shopping item, with array of primary keys of the
 * members instead of an array of objects, in order
 * to pass this object as patch for an existing item
 * to the according endpoint */
export class EditableShoppingItem extends NewShoppingItem {
  _id: number;
  constructor(item: ShoppingItem) {
    super();
    this._id = item._id;
    this.name = item.name;
    this.members = item.members.map((m) => m._id);
  }
}

@Injectable()
export class ShoppingListService {

  private baseURL = 'web-api/v1/';
  private itemsURL = (groupID: number) => {
    return `${this.baseURL}/groups/${groupID}/items/`;
  }
  private itemURL = (groupID: number, itemID: number) => {
    return `${this.baseURL}/groups/${groupID}/items/${itemID}/`;
  }
  private archiveURL = (groupID: number, itemID: number) => {
    return `${this.baseURL}/groups/${groupID}/items/${itemID}/archive`;
  }

  constructor(private http: Http, private apiHelper: ApiHelper, private authService: AuthService) {
  }

  getItems(groupID: number, checked: boolean = false): Observable<ShoppingItem[]> {
    const url = this.itemsURL(groupID);
    const options = this.authService.getAuthOptions();
    const params: URLSearchParams = new URLSearchParams();
    params.set('checked', String(checked));
    options.params = params;
    return this.http.get(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <ShoppingItem[]> data;
      })
      .catch(this.apiHelper.handleError);
  }

  createItem(groupID: number, item: NewShoppingItem): Observable<ShoppingItem> {
    const options = this.authService.getAuthOptions();
    const url = this.itemsURL(groupID);
    return this.http.post(url, item, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <ShoppingItem> data;
      })
      .catch(this.apiHelper.handleError);
  }

  deleteItem(groupID: number, itemID: number): Observable<ShoppingItem> {
    const options = this.authService.getAuthOptions();
    const url = this.itemURL(groupID, itemID);
    return this.http.delete(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <ShoppingItem> data;
      })
      .catch(this.apiHelper.handleError);
  }

  updateItem(groupID: number, item: EditableShoppingItem): Observable<ShoppingItem> {
    const options = this.authService.getAuthOptions();
    const url = this.itemURL(groupID, item._id);
    const body = item;
    return this.http.patch(url, body, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <ShoppingItem> data;
      })
      .catch(this.apiHelper.handleError);
  }

  archiveItem(groupID: number, item: ShoppingItem): Observable<ShoppingItem> {
    const url = this.archiveURL(groupID, item._id);
    const options = this.authService.getAuthOptions();
    return this.http.post(url, {}, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <ShoppingItem> data;
      })
      .catch(this.apiHelper.handleError);
  }
}
