import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { ShoppingListService } from './shopping-list.service';

import { ShoppingListsComponent } from './shopping-lists.component';

import { ShoppingListsRoutingModule } from './shopping-lists-routing.module';
import { SharedModule } from '../shared/shared.module';
import {PaymentService} from '../wallet/wallet.service';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule,
    ShoppingListsRoutingModule,
  ],
  declarations: [
    ShoppingListsComponent,
  ],
  providers: [
    ShoppingListService,
    PaymentService,
  ]
})
export class ShoppingListsModule {}
