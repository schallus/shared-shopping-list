import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ShoppingListsComponent} from './shopping-lists.component';
import {ShoppingListService} from './shopping-list.service';
import { AuthGuard } from '../shared/account/auth-guard.service';


const shoppingListsRouts: Routes = [
  {
    path: 'lists',
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: '',
        component: ShoppingListsComponent
      },
      {
        path: 'group/:id',
        component: ShoppingListsComponent,
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(shoppingListsRouts)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    ShoppingListService
  ]
})
export class ShoppingListsRoutingModule {
}
