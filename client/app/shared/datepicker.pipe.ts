import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'datepicker'})
export class DatepickerPipe implements PipeTransform {
  transform(date: any): Object {
    if (!date.year && !date.month && !date.day) {
      let newDate;
      if (!(date instanceof Date)) {
        newDate = new Date(date);
      } else {
        newDate = date;
      }

      const formattedDate = {
        year: newDate.getFullYear(),
        month: newDate.getMonth() + 1,
        day: newDate.getDate(),
      };
      return formattedDate;
    } else {
      return date;
    }
  }
}
