import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GroupsComponent } from './groups/groups.component';
import {GroupService} from './groups/group.service';
import {RouterModule} from '@angular/router';
import {AccountComponent} from './account/account.component';
import {AuthGuard} from './account/auth-guard.service';
import {AuthService} from './account/auth.service';
import { FormsModule } from '@angular/forms';
import {ApiHelper} from './api-helper/api-helper.service';
import {SpinnerComponent} from './spinner.component';
import {JoinPipe} from './join.pipe';
import {DatepickerPipe} from './datepicker.pipe';
import {InvitationsComponent} from './groups/invitations.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AccountMenuComponent} from './account/account-menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    NgbModule
  ],
  declarations: [
    SpinnerComponent,
    GroupsComponent,
    AccountComponent,
    InvitationsComponent,
    JoinPipe,
    DatepickerPipe,
    AccountMenuComponent,
  ],
  exports: [
    SpinnerComponent,
    GroupsComponent,
    AccountComponent,
    InvitationsComponent,
    JoinPipe,
    DatepickerPipe,
    AccountMenuComponent,
  ],
  providers: [
    GroupService,
    ApiHelper,
    AuthGuard,
    AuthService
  ]
})
export class SharedModule { }
