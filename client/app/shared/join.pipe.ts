import { Pipe, PipeTransform } from '@angular/core';

/*
 * Join array to single string. Optionally a property name may
 * passed, if the array consists of objects.
 * Usage:
 *   array | join:separator
 * Example:
 *   {{ [{name:'foo'}, {name:'bar'}] | join:'name' }}
 *   formats to: foo, bar
 */
@Pipe({name: 'join'})
export class JoinPipe implements PipeTransform {
  transform(array: Array<any>, prop: string): string {
    const sep = ' ';
    return array.reduce((res, item, i) => res += (prop ? item[prop] : item) + (i + 1 < array.length ? sep : ''), '');
  }
}
