import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService, User} from './auth.service';

class LoginUser {
  email: string;
  password: string;
}

@Component({
  templateUrl: './account.component.html'
})
export class AccountComponent {
  error: string;
  processing = false;
  model: LoginUser = new LoginUser();
  user: User;

  constructor(public authService: AuthService, public router: Router) {
    this.user = authService.user;
    authService.userStream.subscribe((user) => {
      this.user = user;
    });
  }

  onSubmit() {
    this.processing = true;
    this.authService.login(this.model.email, this.model.password).subscribe(() => {
      const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/';
      // Redirect the user
      this.router.navigate([redirect]);
      this.processing = false;
    }, (error) => {
      this.error = error.message;
      this.processing = false;
    });
  }

  logout() {
    this.authService.logout();
  }
}
