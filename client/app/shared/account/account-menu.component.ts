import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService, User} from './auth.service';


@Component({
  selector: 'app-account-menu',
  templateUrl: './account-menu.component.html',
})
export class AccountMenuComponent {

  user: User;

  constructor(public authService: AuthService, public router: Router) {
    this.user = authService.user;
    authService.userStream.subscribe((user) => {
      this.user = user;
    });
  }
}
