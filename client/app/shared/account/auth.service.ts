import {Injectable, OnInit} from '@angular/core';
import {Http, RequestOptions, Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {Subscription} from 'rxjs/Subscription';
import {ApiHelper} from '../api-helper/api-helper.service';

declare const moment: any;

export class User {
  _id: number;
  firstname: string;
  lastname: string;
  email: string;
}

@Injectable()
export class AuthService {
  // store the URL so we can redirect after logging in
  redirectUrl: string;
  user: User;
  userStream = new Subject<User>();

  private expire: Date;
  private tokenRefreshSub: Subscription;

  private baseURL = 'web-api/v1/';
  private authURL = this.baseURL + 'authenticate/';

  constructor(private http: Http, private apiHelper: ApiHelper) {
    // get auth info from locale storage
    const token = localStorage.getItem('token');
    this.expire = new Date(localStorage.getItem('expire'));
    this.user = JSON.parse(localStorage.getItem('user'));
    // reset auth service, if info is missing or token has expired
    if (!this.getToken() || !this.user || !this.expire || this.expire < new Date()) {
      this.logout();
    } else {
      // else -> auth info is valid -> refresh token, before it expires
      this.userStream.next(this.user);
      this.setupTokenRefresher(this.expire, token);
    }
  }

  login(email: string, password: string): Observable<User> {
    return this.authenticate({email, password});
  }

  logout(): void {
    this.resetTokenRefresher();
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('expire');
    this.user = null;
    this.userStream.next(null);
    console.log('Logged out');
  }

  isAuthenticated(): boolean {
    return !!this.getToken() && !!this.user; // same as: return this.token ? true : false
  }

  /** Request access token and user information for given credentials
   * or a not yet expired access token */
  private authenticate(authData: Object): Observable<User> {
    return this.http.post(this.authURL, authData)
      .map(this.apiHelper.extractData)
      .map((data) => {
        // set properties
        const token = data.token;
        this.user = <User> data.user;
        this.expire = new Date(data.expire);
        // update local storage
        localStorage.setItem('token', token);
        localStorage.setItem('user', JSON.stringify(this.user));
        localStorage.setItem('expire', this.expire.toString());
        // refresh token before it expires
        this.setupTokenRefresher(this.expire, token);
        this.userStream.next(this.user);
        return this.user;
      })
      .catch(this.apiHelper.handleError);
  }

  private resetTokenRefresher() {
    if (this.tokenRefreshSub) {
      this.tokenRefreshSub.unsubscribe();
      this.tokenRefreshSub = null;
    }
  }

  private setupTokenRefresher(date: Date, token: string) {
    console.log('Token refresher initialized for date: ' + date.toLocaleString());
    this.resetTokenRefresher();
    // refresh token, 1min before the token expires
    const refreshTime = moment(date).subtract(1, 'm').toDate();
    // const refreshTime = moment().add(3, 'second').toDate(); // for tests
    this.tokenRefreshSub = Observable.timer(refreshTime)
      .subscribe(() => {
        console.log('Trying to refresh access token...');
        this.authenticate({token}).subscribe((user) => {
          console.log('Refreshing token succeeded.');
        });
      });
  }

  private getToken(): string {
    return localStorage.getItem('token') || null;
  }

  getAuthOptions(): RequestOptions {
    const headers = new Headers();
    headers.append('x-access-token', this.getToken());
    return new RequestOptions({headers});
  }
}
