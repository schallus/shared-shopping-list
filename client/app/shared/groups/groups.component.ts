import {Component, OnInit, OnDestroy, Input, ElementRef, ViewChild, ViewContainerRef} from '@angular/core';
import {EditableGroup, Group, GroupService, Invitation, Member, NewGroup} from './group.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/retry';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastsManager} from 'ng2-toastr';
import {APIError} from '../api-helper/api-helper.service';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',

})
export class GroupsComponent implements OnInit {

  // ----- VIEW REFERENCES -----
  @ViewChild('invitationsPopover') invitationsPopover;
  @ViewChild('invitationsBtn') invitationsBtn;
  @ViewChild('groupModal') itemModal: ElementRef;

  modal: NgbModalRef;

  // ----- FETCHED DATA -----
  groups: Group[];
  selected: Group;
  invitationCount: number;

  // ----- EDIT AND CREATE VARIABLES -----
  newGroup: NewGroup = new NewGroup();
  editGroup: NewGroup;
  editGroupMembers: Member[];
  email: string;

  // util
  editRequestRunning = false;
  invitationRequestRunning = false;

  // ----- INVITATIONS ------
  invitationsOpen = false;

  @Input() parentRoute: string;

  constructor(private service: GroupService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private toastr: ToastsManager,
              private router: Router) {
    document.addEventListener('click', this.openCloseInvitations.bind(this));
  }

  ngOnInit() {
    // fetch groups
    this.service.getGroups()
      .subscribe((groups) => {
        this.groups = groups;
        // navigate to the first group, if no group is selected
        const groupID = this.route.snapshot.params['id'];
        console.log(!!groupID);
        if (!groupID && groups.length > 0) {
          this.router.navigate([this.parentRoute + '/group/', groups[0]._id]);
        }
      }, (err) => {
        this.handleError(err);
      });
    // fetch selected group
    this.route.params
      .map((params) => +params['id'])
      .skipWhile((id) => isNaN(id))
      .switchMap((id) => {
        const groups = this.service.getGroups();
        const selected = this.service.getGroup(id);
        return Observable.forkJoin([groups, selected]);
      })
      .subscribe((result) => {
        this.groups = result[0];
        this.selected = result[1];
      }, (err) => {
        this.handleError(err);
      });
    // fetch invitations
    this.service.getInvitations().map((invitations) => invitations.length)
      .subscribe((invitationCount) => {
        this.invitationCount = invitationCount;
      }, (err) => {
        this.handleError(err);
      });
  }

  // --------- CRUD OPERATIONS ---------

  addorUpdateGroup(group: NewGroup) {
    this.editRequestRunning = true;
    new Promise((resolve, reject) => {
      if (!group.name.trim()) {
        return reject('Please provide a group name');
      }
      if (this.groupIsEditable(group)) {
        // UPDATE GRUP
        const editableGroup = <EditableGroup> group;
        this.service.updateGroup(editableGroup)
          .subscribe((updatedGroup) => {
            this.displayUpdate(updatedGroup);
            this.toastr.info(`Updated group ${updatedGroup.name}`);
            this.resetEditGroup();
            resolve();
          }, (err) => {
            this.editRequestRunning = false;
            this.handleError(err);
          });
      } else {
        const newGroup = this.editGroup;
        if (!newGroup.image) {
          return reject('Please select a group image');
        }
        this.service.addGroup(newGroup).subscribe((createdGroup) => {
          this.toastr.success(`Created group ${createdGroup.name}`);
          this.displayGroup(createdGroup);
          this.resetEditGroup();
          resolve();
        }, (err) => {
          this.handleError(err);
          this.editRequestRunning = false;
        });
      }
    }).then(() => {
      this.editRequestRunning = false;
      this.closeModal();
    }).catch((msg) => {
      this.toastr.error(msg);
      this.editRequestRunning = false;
    });
  }

  // --------- HELPER METHODS ---------

  resetEditGroup() {
    this.editGroup = new NewGroup();
    this.editGroupMembers = null;
  }

  // --------- ACTION METHODS ---------

  onEditBtnClicked(group: Group) {
    const editableGroup = new EditableGroup(group);
    this.editGroupMembers = group.members;
    this.showEditDialog(editableGroup);
  }

  onFileChange(event) {
    this.editGroup.image = event.srcElement.files[0];
  }

  invite(member: string) {
    this.invitationRequestRunning = true;
    if (!member) {
      this.invitationRequestRunning = false;
      return this.toastr.error('Please enter a email address');
    }
    this.service.invite(this.selected._id, member).subscribe(
      (invitation) => {
        this.invitationRequestRunning = false;
        const receiver = invitation.user;
        this.toastr.success(`Invitation sent to ${receiver.firstname} ${receiver.lastname}`);
        this.email = null;
      }, (err) => {
        this.invitationRequestRunning = false;
        this.handleError(err);
      });
  }

  leaveGroup(group: Group) {
    this.service.leaveGroup(group._id).subscribe(() => {
      this.modal.close();
      this.router.navigate(['/']);
    }, (err) => {
      this.handleError(err);
    });
  }

  openCloseInvitations(event: any) {
    if (!this.invitationsOpen && this.invitationsBtn.nativeElement.contains(event.target)) {
      this.invitationsOpen = true;
    } else if (this.invitationsOpen && !this.invitationsPopover.nativeElement.contains(event.target)) {
      this.invitationsOpen = false;
    }
  }

  refreshInvitationCount(inv: Invitation[]) {
    if (this.invitationCount > inv.length && inv.length === 0) {
      this.invitationsOpen = false;
    }
    this.invitationCount = inv.length;
  }

  // --------- UI HELPER FUNCTIONS ---------

  displayUpdate(group: Group) {
    if (group._id === this.selected._id) {
      this.selected = group;
    }
    for (let i = 0; i < this.groups.length; i++) {
      if (this.groups[i]._id === group._id) {
        this.groups[i] = group;
      }
    }
  }

  displayGroup(group: Group) {
    this.groups.push(group);
    this.router.navigate([this.parentRoute + '/group/', group._id]);
  }

  showEditDialog(group: NewGroup) {
    this.editGroup = group;
    this.openModal(this.itemModal);
  }

  private openModal(content) {
    this.modal = this.modalService.open(content);
  }

  private closeModal() {
    if (this.modal) {
      this.modal.close();
    }
  }

  // --------- BOOLEAN FUNCTIONS ---------

  private groupIsEditable(group): boolean {
    return !!group._id;
  }

  // --------- ERROR HANDLER ---------

  private handleError(err: APIError) {
    if (err.status === 404) {
      this.toastr.error(err.message);
      // this.router.navigate(['/']);
    } else {
      this.toastr.error(err.message);
    }
  }
}

