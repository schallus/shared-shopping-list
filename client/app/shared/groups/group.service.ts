import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import {Http} from '@angular/http';
import {ApiHelper} from '../api-helper/api-helper.service';
import {AuthService} from '../account/auth.service';
import {modelGroupProvider} from '@angular/forms/src/directives/ng_model_group';


export class PartialGroup {
  _id: number;
  name: string;
}

export class Group extends PartialGroup {
  date: Date;
  image: string;
  thumbnail: string;
  members: Member[];
}

export class  NewGroup extends PartialGroup {
  members: number[];
  image: File;
}

export class EditableGroup extends PartialGroup {
  image: File;
  members: number[];
  constructor(group: Group) {
    super();
    this._id = group._id;
    this.name = group.name;
    this.image = null;
    this.members = group.members.map((g) => g._id);
  }
}

export class Member {
  _id: number;
  firstname: string;
  lastname: string;
  email: string;
}

export class Invitation {
  _id: number;
  sender: Member;
  date: Date;
  user: Member;
  group: PartialGroup;
}


@Injectable()
export class GroupService {

  // URLs
  private baseURL = 'web-api/v1/';
  private groupsURL = this.baseURL + 'groups/';
  private invitationsURL = this.groupsURL + 'invitations/';
  private groupURL = (groupID) => {
    return `${this.groupsURL}${groupID}/`;
  }
  private leaveURL = (groupID) => {
    return this.groupURL(groupID) + 'leave/';
  }
  private inviteURL = (groupID) => {
    return this.groupURL(groupID) + 'invite/';
  }
  private acceptInvitationURL = (groupID) => {
    return this.groupURL(groupID) + 'accept/';
  }
  private rejectInvitationURL = (groupID) => {
    return this.groupURL(groupID) + 'reject/';
  }

  constructor(private http: Http, private apiHelper: ApiHelper, private authService: AuthService) {
  }

  getGroups(): Observable<Group[]> {
    const url = this.groupsURL;
    const options = this.authService.getAuthOptions();
    return this.http.get(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Group[]> data;
      })
      .catch(this.apiHelper.handleError);
  }

  getGroup(groupID: number): Observable<Group> {
    const url = this.groupURL(groupID);
    const options = this.authService.getAuthOptions();
    return this.http.get(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Group> data;
      })
      .catch(this.apiHelper.handleError);
  }

  addGroup(group: NewGroup): Observable<Group> {
    const url = this.groupsURL;
    const options = this.authService.getAuthOptions();
    const formData = new FormData();
    formData.append('image', group.image);
    formData.append('name', group.name);
    return this.http.post(url, formData, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Group> data;
      })
      .catch(this.apiHelper.handleError);
  }

  updateGroup(group: EditableGroup): Observable<Group> {
    const url = this.groupURL(group._id);
    const options = this.authService.getAuthOptions();
    const formData = new FormData();
    formData.append('image', group.image);
    formData.append('name', group.name);
    return this.http.patch(url, formData, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Group> data;
      })
      .catch(this.apiHelper.handleError);
  }
  leaveGroup(groupID: number): Observable<Response> {
    const url = this.leaveURL(groupID);
    const options = this.authService.getAuthOptions();
    return this.http.post(url, {}, options)
      .map(this.apiHelper.extractData)
      .catch(this.apiHelper.handleError);
  }

  getInvitations(): Observable<Invitation[]> {
    const url = this.invitationsURL;
    const options = this.authService.getAuthOptions();
    return this.http.get(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Invitation[]> data;
      })
      .catch(this.apiHelper.handleError);
  }

  invite(groupID: number, email: string): Observable<Invitation> {
    const url = this.inviteURL(groupID);
    const options = this.authService.getAuthOptions();
    return this.http.post(url, {email}, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Invitation> data;
      })
      .catch(this.apiHelper.handleError);
  }

  acceptInvitation(groupID: number): Observable<Group> {
    const url = this.acceptInvitationURL(groupID);
    const options = this.authService.getAuthOptions();
    return this.http.post(url, {}, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Group> data;
      })
      .catch(this.apiHelper.handleError);
  }

  rejectInvitation(groupID: number): Observable<Response> {
    const url = this.rejectInvitationURL(groupID);
    const options = this.authService.getAuthOptions();
    return this.http.post(url, {}, options)
      .map(this.apiHelper.extractData)
      .catch(this.apiHelper.handleError);
  }
}
