import {Group, GroupService, Invitation} from './group.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Component, EventEmitter, OnInit, Output, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';

@Component({
  selector: 'app-invitations',
  templateUrl: './invitations.component.html',
})
export class InvitationsComponent implements OnInit {

  @Output() onInvitationChange = new EventEmitter<Invitation[]>();
  invitations: Invitation[];

  constructor(private service: GroupService,
              private toastr: ToastsManager,
              private router: Router) {
  }

  ngOnInit() {
    this.service.getInvitations()
      .subscribe((invitations) => {
        this.invitations = invitations;
        this.onInvitationChange.emit(this.invitations);
      });
  }

  onAccept(invitation: Invitation) {
    this.service.acceptInvitation(invitation.group._id)
      .subscribe((group) => {
        this.removeInvitation(invitation);
        this.router.navigate(['/lists/group/', group._id]);
      }, (err) => {
        this.toastr.error(err.message);
      });
  }

  onReject(invitation: Invitation) {
    this.service.rejectInvitation(invitation.group._id)
      .subscribe(() => {
        this.toastr.info('Invitation rejected');
        this.removeInvitation(invitation);
      }, (err) => {
        this.toastr.error(err.message);
      });
  }

  private removeInvitation(invitation: Invitation) {
    for (let i = 0; i < this.invitations.length; i++) {
      if (this.invitations[i]._id === invitation._id) {
        this.invitations.splice(i, 1);
        this.onInvitationChange.emit(this.invitations);
        return true;
      }
    }
  }
}
