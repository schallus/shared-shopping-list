import { Component } from '@angular/core';

@Component({
  template: `<div class="container"><br/><br/><h1>404 - Page not found</h1></div>`
})
export class PageNotFoundComponent {}
