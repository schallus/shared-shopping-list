import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WalletComponent } from './wallet/wallet.component';
import { PageNotFoundComponent } from './not-found.component';
import {AccountComponent} from './shared/account/account.component';

const appRoutes: Routes = [
  { path: 'account', component: AccountComponent },
  { path: '', redirectTo: '/lists', pathMatch: 'full' }, // root
  { path: '**', component: PageNotFoundComponent }  // default: 404
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
