import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-menu',
  template: `
    <nav>
      <ul>
        <li class="p-2">
          <a routerLink="/wallet" routerLinkActive="active" class="nav-item">Wallet</a>
        </li>
        <li class="p-2">
          <a routerLink="/lists" routerLinkActive="active" class="nav-item">Shoppinglists</a>
        </li>
      </ul>
    </nav>
  `,
})
export class MenuComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
