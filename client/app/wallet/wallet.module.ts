import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { PaymentService } from './wallet.service';

import { WalletComponent } from './wallet.component';

import { WalletRoutingModule } from './wallet-routing.module';
import { SharedModule } from '../shared/shared.module';
import {ApiHelper} from '../shared/api-helper/api-helper.service';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    SharedModule,
    WalletRoutingModule
  ],
  declarations: [
    WalletComponent,
  ],
  providers: [
    PaymentService,
  ]
})
export class WalletModule {}
