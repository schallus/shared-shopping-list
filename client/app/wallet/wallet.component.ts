import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Group, GroupService} from '../shared/groups/group.service';
import {ShoppingListService} from '../shopping-lists/shopping-list.service';
import {AuthService} from '../shared/account/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {ToastsManager} from 'ng2-toastr';
import {Balance, EditablePayment, NewPayment, Payment, PaymentService} from './wallet.service';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/skipWhile';
import 'rxjs/add/observable/empty';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/forkJoin';
import {Observable} from 'rxjs/Observable';

// declare const moment: any;

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
})
export class WalletComponent implements OnInit {

  // ----- VIEW REFERENCES -----
  @ViewChild('paymentModal') paymentModal: ElementRef;
  modal: NgbModalRef;

  // ----- EDIT AND CREATE VARIABLES -----
  // Models
  newPayment: NewPayment = new NewPayment();
  editPayment: NewPayment;

  group: Group;
  groupMembers: number[];
  payments: Payment[] = [];
  balances: Balance[];
  editRequestRunning = false;

  constructor(private groupService: GroupService,
              private paymentService: PaymentService,
              private authService: AuthService,
              private route: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal,
              public toastr: ToastsManager,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.route.params
      .map((params) => +params['id'])
      .skipWhile((id) => isNaN(id))
      .switchMap((groupID) => {
        const group = this.groupService.getGroup(groupID);
        const payments = this.paymentService.getPayments(groupID);
        return Observable.forkJoin([group, payments]);
      })
      .subscribe((results) => {
        // this.today = moment().format('YYYY-MM-DD');
        // console.log(this.today);

        this.group = results[0];
        // extract array of member ids
        this.groupMembers = this.group.members.map((m) => m._id);

        this.payments = results[1];

        this.resetNewPayment();
      }, (err) => {
        this.handleError(err);
      });
    this.route.params
      .map((params) => +params['id'])
      .skipWhile((id) => isNaN(id))
      .switchMap((id) => {
        return this.paymentService.getBalances(id);
      }).subscribe((balances) => {
          this.balances = balances;
      });
  }


  private handleError(err) {
    if (err.status === 403) {
      this.toastr.error(err.message);
      // this.router.navigate(['/']);
    } else {
      this.toastr.error(err.message);
    }
  }

  private getPaymentTitle(payment: Payment) {
    if (payment.title) {
      return payment.title;
    } else {
      return payment.items.map((item) => item.name).join(', ');
    }
  }

  private parseDate(date) {
    if (date.year && date.month && date.day) {
      return new Date(`${date.year}-${date.month}-${date.day}`);
    } else {
      return new Date(date);
    }
  }

  refreshPayments() {
    const groupID = this.group._id;
    this.paymentService.getPayments(groupID)
      .subscribe((updatedList) => {
        this.payments = updatedList;
        this.toastr.info('Payment list updated.');
      }, (err) => {
        this.handleError(err);
      });
  }

  private refreshBalances() {
    const groupID = this.group._id;
    this.paymentService.getBalances(groupID)
      .subscribe((updatedBalances) => {
        this.balances = updatedBalances;
      }, (err) => {
        this.handleError(err);
      });
  }

  addOrUpdatePayment(payment: NewPayment) {
    this.editRequestRunning = true;
    const groupID = this.group._id;

    // Parse date
    payment.date = this.parseDate(payment.date);

    new Promise((resolve, reject) => {
      // Update
      if (this.paymentIsEditable(payment)) {
        const editablePayment = <EditablePayment> payment;
        if (!payment.date ) {
          return reject('Please enter a date.');
        }
        if (!payment.amount) {
          payment.amount = 0;
        }
        this.paymentService.updatePayment(groupID, editablePayment)
          .subscribe((updatedPayment) => {
            this.displayUpdate(updatedPayment);
            this.refreshBalances();
            this.closeModal();
          }, (err) => {
            this.handleError(err);
          }, () => {
            resolve('Payment successfully updated.');
          });
        // Add
      } else {
        // If no member, we select them all
        if (!payment.members) {
          payment.members = this.groupMembers;
        }
        if (!payment.date) {
          payment.date = new Date();
        }
        if (!payment.title || !payment.amount) {
          return reject('Please enter a title and a price.');
        }
        this.paymentService.createPayment(groupID, payment)
          .subscribe((newPayment) => {
            this.displayUpdate(newPayment);
            this.refreshBalances();
            this.closeModal();
          }, (err) => {
            this.handleError(err);
          }, () => {
            return resolve(`Payment successfully created.`);
          });
      }
    }).then((msg: string) => {
      this.toastr.info(msg);
      this.editRequestRunning = false;
      // Reset new payment
      this.resetNewPayment();
    }).catch((err) => {
      this.toastr.error(err);
      this.editRequestRunning = false;
    });
  }

  private deletePayment(payment: EditablePayment) {
    this.editRequestRunning = true;
    this.paymentService.deletePayment(this.group._id, payment._id)
      .subscribe((deletedPayment) => {
        this.editRequestRunning = false;
        if (deletedPayment.title) {
          this.toastr.info(`Deleted ${deletedPayment.title}`);
        } else {
          this.toastr.info('The payment has been deleted.');
        }
        if (!this.hidePayment(deletedPayment)) {
          // deleting failed
          console.log('The payment was already removed from the list');
        }
        this.refreshBalances();
        this.closeModal();
      }, (err) => {
        this.editRequestRunning = false;
        this.handleError(err);
      });
  }

  private paymentIsEditable(payment): boolean {
    return !!payment._id;
  }

  private displayUpdate(updatedPayment: Payment): boolean {
    for (let i = 0; i < this.payments.length; i++) {
      if (this.payments[i]._id === updatedPayment._id) {
        this.payments[i] = updatedPayment;
        return true;
      }
    }
    this.payments.push(updatedPayment)
    return true;
  }

  private hidePayment(removedPayment: Payment): boolean {
    for (let i = 0; i < this.payments.length; i++) {
      if (this.payments[i]._id === removedPayment._id) {
        this.payments.splice(i, 1);
        return true;
      }
    }
    return false;
  }

  private openModal(content) {
    this.modal = this.modalService.open(content);
  }

  private closeModal() {
    if (this.modal) {
      this.modal.close();
    }
  }

  // --------- ACTION METHODS ---------

  showEditDialog(payment: NewPayment) {
    this.editPayment = payment;
    this.openModal(this.paymentModal);
  }

  onPaymentClick(payment: Payment) {
    const editablePayment = new EditablePayment(payment);
    this.showEditDialog(editablePayment);
  }

  private resetNewPayment() {
    this.newPayment = {
      title: null,
      date: new Date(),
      amount: null,
      members: [...this.groupMembers], // copy groupMembers
      items: null,
    };
  }
}
