import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions, URLSearchParams} from '@angular/http';
import {Group, Member} from '../shared/groups/group.service';
import {Observable} from 'rxjs/Observable';
import {ApiHelper} from '../shared/api-helper/api-helper.service';
import {AuthService} from '../shared/account/auth.service';
import {ShoppingItem} from '../shopping-lists/shopping-list.service';

export class Payment {
  _id: number;
  title: string;
  creator: Member;
  group: Group;
  edited: Date;
  date: Date;
  amount: number;
  items: ShoppingItem[];
}

export class NewPayment {
  title: string;
  date: Date;
  amount: number;
  items: number[];
  members: number[];
}

export class EditablePayment extends NewPayment {
  _id: number;
  constructor(payment: Payment) {
    super();
    this._id = payment._id;
    this.title = payment.title;
    this.date = payment.date;
    this.amount = payment.amount;
    this.items = null;
    this.members = null;
  }
}

export class Balance {
  user: Member;
  balance: number;
}

@Injectable()
export class PaymentService {
  private baseURL = 'web-api/v1/';
  private paymentsURL = (groupID) => {
    return `${this.baseURL}groups/${groupID}/payments`;
  }
  private paymentURL = (groupID: number, paymentID: number) => {
    return `${this.baseURL}groups/${groupID}/payments/${paymentID}`;
  }

  constructor(private http: Http, private apiHelper: ApiHelper, private authService: AuthService) {
  }

  getPayments(groupID: number): Observable<Payment[]> {
    const url = this.paymentsURL(groupID);
    const options = this.authService.getAuthOptions();
    return this.http.get(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Payment[]> data;
      })
      .catch(this.apiHelper.handleError);
  }

  createPayment(groupID: number, payment: NewPayment): Observable<Payment> {
    const options = this.authService.getAuthOptions();
    const url = this.paymentsURL(groupID);
    const body = payment;
    // return this.http.post()
    return this.http.post(url, body, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Payment> data;
      })
      .catch(this.apiHelper.handleError);
  }

  deletePayment(groupID: number, paymentID: number, keepChecked: boolean = false): Observable<Payment> {
    const options = this.authService.getAuthOptions();
    const params: URLSearchParams = new URLSearchParams();
    (keepChecked) ? params.set('keepChecked', 'true') : params.set('keepChecked', 'false');
    options.params = params;
    const url = this.paymentURL(groupID, paymentID);
    return this.http.delete(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Payment> data;
      })
      .catch(this.apiHelper.handleError);
  }

  updatePayment(groupID: number, payment: EditablePayment): Observable<Payment> {
    const options = this.authService.getAuthOptions();
    const url = this.paymentURL(groupID, payment._id);
    const body = payment;
    return this.http.put(url, body, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Payment> data;
      })
      .catch(this.apiHelper.handleError);
  }

  getBalances(groupID: number): Observable<Balance[]> {
    const url = `${this.baseURL}/groups/${groupID}/balance`;
    const options = this.authService.getAuthOptions();
    return this.http.get(url, options)
      .map(this.apiHelper.extractData)
      .map((data) => {
        return <Balance[]> data;
      })
      .catch(this.apiHelper.handleError);
  }
}
