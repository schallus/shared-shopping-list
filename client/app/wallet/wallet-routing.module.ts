import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { AuthGuard } from '../shared/account/auth-guard.service';
import {PaymentService} from './wallet.service';
import {WalletComponent} from './wallet.component';


const walletRoutes: Routes = [
  {
    path: 'wallet',
    canActivate: [ AuthGuard ],
    canActivateChild: [ AuthGuard ],
    children: [
      {
        path: '',
        component: WalletComponent
      },
      {
        path: 'group/:id',
        component: WalletComponent,
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(walletRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    PaymentService
  ]
})
export class WalletRoutingModule {
}
