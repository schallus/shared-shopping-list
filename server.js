'use strict';

// ----- LOAD ENV VARIABLES -----

require('dotenv').config();

// ----- REQUIREMENTS -----

// vendor
const express = require('express');
const path = require('path');
const helmet = require('helmet'); // Helps you secure your Express apps
                                  // by setting various HTTP headers
const cors = require('cors'); // Enable All CORS Requests

// custom
const db = require('./server/modules/database');
const apiRouter = require('./server/routes/api');
const authRouter = require('./server/routes/auth');

const app = express();

// ----- MIDDLEWARE -----

app.use(express.static(path.join(__dirname, 'dist')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use(helmet());

// ----- VIEW ENGINE -----

app.set('view engine', 'pug');
app.set('views', './server/views');

// ----- MIDDLEWARES -----

const ensureSecure = (req, res, next) => {
  if(process.env.NODE_ENV === 'prod') {
    if (req.headers['x-forwarded-proto']
      && req.headers['x-forwarded-proto'] === 'https') {
      // OK, continue
      return next();
    }
    // handle port numbers if you need non defaults
    res.redirect('https://' + req.hostname + req.url);
  } else if(process.env.NODE_ENV === 'dev') {
    if(req.secure) {
      // OK, continue
      return next();
    }
    // handle port numbers if you need non defaults
    res.redirect('https://' + req.hostname + req.url);
  }
};

// ----- ROUTES -----

app.use(ensureSecure);
app.use(authRouter);
app.use('/web-api/v1', apiRouter);

app.all('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

// ----- ENTRY POINT -----

db.connect().then(() => {
  console.log('Database connection established');
  if(process.env.NODE_ENV === 'prod') {
    const PORT = process.env.PORT || 3000;
    app.listen(PORT);
  } else if(process.env.NODE_ENV === 'dev') {
    const https = require('https');
    const http = require('http');
    const fs = require('fs');

    const sslKey = fs.readFileSync('ssl-key.pem');
    const sslCert = fs.readFileSync('ssl-cert.pem');

    const sslOptions = {
      key: sslKey,
      cert: sslCert,
    };

    http.createServer(app).listen(3000);
    https.createServer(sslOptions, app).listen(4000);
  }
  console.log('Application listening');
}, (err) => {
  console.log('Database connection failed: ' + err);
});
